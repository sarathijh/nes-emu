cmake_minimum_required(VERSION 3.15)

project(mos6502)

# Enable code coverage for unit tests
#set(CMAKE_CXX_FLAGS "--coverage")

# Define the library name and source files
add_library(${PROJECT_NAME}
        src/mos6502.cpp
        src/mos6502_addressing_modes.cpp
        src/mos6502_instructions.cpp
        src/mos6502_stack.cpp
        src/mos6502_memory.cpp
        )

# Add our public headers to the library
target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/include)

# Use C++20
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_STANDARD 20)

# Enable all warnings
target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra -Werror)
