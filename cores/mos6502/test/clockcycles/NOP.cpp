#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("NOP clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xEA) == 2);
}
