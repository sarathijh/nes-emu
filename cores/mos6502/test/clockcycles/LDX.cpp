#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("LDX immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA2) == 2);
}

TEST_CASE("LDX zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA6) == 3);
}

TEST_CASE("LDX zero page y clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xB6) == 4);
}

TEST_CASE("LDX absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xAE) == 4);
}

TEST_CASE("LDX absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xBE, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("LDX absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xBE, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}
