#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("BIT zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x24) == 3);
}

TEST_CASE("BIT absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x2C) == 4);
}
