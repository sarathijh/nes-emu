#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("JSR clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x20) == 6);
}

TEST_CASE("RTI clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x40) == 6);
}

TEST_CASE("RTS clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x60) == 6);
}
