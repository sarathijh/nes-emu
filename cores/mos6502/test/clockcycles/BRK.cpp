#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("BRK clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x00) == 7);
}
