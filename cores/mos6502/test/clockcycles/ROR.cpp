#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("ROR accumulator clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x6A) == 2);
}

TEST_CASE("ROR zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x66) == 5);
}

TEST_CASE("ROR zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x76) == 6);
}

TEST_CASE("ROR absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x6E) == 6);
}

TEST_CASE("ROR absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x7E) == 7);
}
