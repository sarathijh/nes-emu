#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("DEC zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC6) == 5);
}

TEST_CASE("DEC zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xD6) == 6);
}

TEST_CASE("DEC absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xCE) == 6);
}

TEST_CASE("DEC absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xDE) == 7);
}
