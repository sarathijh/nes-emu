#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("STY zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x84) == 3);
}

TEST_CASE("STY zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x94) == 4);
}

TEST_CASE("STY absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x8C) == 4);
}
