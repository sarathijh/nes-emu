#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("ORA immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x09) == 2);
}

TEST_CASE("ORA zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x05) == 3);
}

TEST_CASE("ORA zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x15) == 4);
}

TEST_CASE("ORA absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x0D) == 4);
}

TEST_CASE("ORA absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x1D, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("ORA absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x1D, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("ORA absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x19, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("ORA absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x19, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("ORA indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x01) == 6);
}

TEST_CASE("ORA indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x11, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("ORA indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x11, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
