#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("TXS clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x9A) == 2);
}

TEST_CASE("TSX clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xBA) == 2);
}

TEST_CASE("PHA clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x48) == 3);
}

TEST_CASE("PLA clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x68) == 4);
}

TEST_CASE("PHP clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x08) == 3);
}

TEST_CASE("PLP clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x28) == 4);
}
