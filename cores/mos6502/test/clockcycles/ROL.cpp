#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("ROL accumulator clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x2A) == 2);
}

TEST_CASE("ROL zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x26) == 5);
}

TEST_CASE("ROL zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x36) == 6);
}

TEST_CASE("ROL absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x2E) == 6);
}

TEST_CASE("ROL absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x3E) == 7);
}
