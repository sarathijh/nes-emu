#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("EOR immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x49) == 2);
}

TEST_CASE("EOR zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x45) == 3);
}

TEST_CASE("EOR zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x55) == 4);
}

TEST_CASE("EOR absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x4D) == 4);
}

TEST_CASE("EOR absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x5D, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("EOR absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x5D, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("EOR absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x59, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("EOR absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x59, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("EOR indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x41) == 6);
}

TEST_CASE("EOR indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x51, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("EOR indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x51, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
