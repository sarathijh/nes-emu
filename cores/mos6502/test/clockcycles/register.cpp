#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("TAX clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xAA) == 2);
}

TEST_CASE("TXA clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x8A) == 2);
}

TEST_CASE("DEX clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xCA) == 2);
}

TEST_CASE("INX clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE8) == 2);
}

TEST_CASE("TAY clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA8) == 2);
}

TEST_CASE("TYA clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x98) == 2);
}

TEST_CASE("DEY clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x88) == 2);
}

TEST_CASE("INY clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC8) == 2);
}
