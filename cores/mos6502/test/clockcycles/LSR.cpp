#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("LSR accumulator clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x4A) == 2);
}

TEST_CASE("LSR zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x46) == 5);
}

TEST_CASE("LSR zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x56) == 6);
}

TEST_CASE("LSR absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x4E) == 6);
}

TEST_CASE("LSR absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x5E) == 7);
}
