#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("LDA immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA9) == 2);
}

TEST_CASE("LDA zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA5) == 3);
}

TEST_CASE("LDA zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xB5) == 4);
}

TEST_CASE("LDA absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xAD) == 4);
}

TEST_CASE("LDA absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xBD, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("LDA absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xBD, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("LDA absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xB9, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("LDA absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xB9, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("LDA indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA1) == 6);
}

TEST_CASE("LDA indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xB1, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("LDA indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xB1, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
