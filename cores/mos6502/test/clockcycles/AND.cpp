#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("AND immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x29) == 2);
}

TEST_CASE("AND zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x25) == 3);
}

TEST_CASE("AND zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x35) == 4);
}

TEST_CASE("AND absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x2D) == 4);
}

TEST_CASE("AND absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x3D, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("AND absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x3D, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("AND absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x39, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("AND absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x39, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("AND indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x21) == 6);
}

TEST_CASE("AND indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x31, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("AND indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x31, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
