#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("STA zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x85) == 3);
}

TEST_CASE("STA zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x95) == 4);
}

TEST_CASE("STA absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x8D) == 4);
}

TEST_CASE("STA absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x9D) == 5);
}

TEST_CASE("STA absolute y clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x99) == 5);
}

TEST_CASE("STA indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x81) == 6);
}

TEST_CASE("STA indirect y clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x91) == 6);
}
