#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("SBC immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE9) == 2);
}

TEST_CASE("SBC zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE5) == 3);
}

TEST_CASE("SBC zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xF5) == 4);
}

TEST_CASE("SBC absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xED) == 4);
}

TEST_CASE("SBC absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xFD, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("SBC absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xFD, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("SBC absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xF9, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("SBC absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xF9, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("SBC indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE1) == 6);
}

TEST_CASE("SBC indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xF1, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("SBC indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xF1, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
