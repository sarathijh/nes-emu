#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("CMP immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC9) == 2);
}

TEST_CASE("CMP zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC5) == 3);
}

TEST_CASE("CMP zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xD5) == 4);
}

TEST_CASE("CMP absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xCD) == 4);
}

TEST_CASE("CMP absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xDD, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("CMP absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xDD, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("CMP absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xD9, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("CMP absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xD9, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("CMP indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC1) == 6);
}

TEST_CASE("CMP indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xD1, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("CMP indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0xD1, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
