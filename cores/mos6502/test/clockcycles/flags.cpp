#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("CLC clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x18) == 2);
}

TEST_CASE("SEC clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x38) == 2);
}

TEST_CASE("CLI clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x58) == 2);
}

TEST_CASE("SEI clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x78) == 2);
}

TEST_CASE("CLV clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xB8) == 2);
}

TEST_CASE("CLD clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xD8) == 2);
}

TEST_CASE("SED clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xF8) == 2);
}
