#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("ADC immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x69) == 2);
}

TEST_CASE("ADC zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x65) == 3);
}

TEST_CASE("ADC zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x75) == 4);
}

TEST_CASE("ADC absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x6D) == 4);
}

TEST_CASE("ADC absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x7D, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("ADC absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0x7D, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("ADC absolute y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x79, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("ADC absolute y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x79, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("ADC indirect x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x61) == 6);
}

TEST_CASE("ADC indirect y (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x71, 0x00, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}

TEST_CASE("ADC indirect y (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA0, 0xAA, 0x71, 0x00, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 6);
}
