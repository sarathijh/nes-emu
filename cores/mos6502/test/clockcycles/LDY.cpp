#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("LDY immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA0) == 2);
}

TEST_CASE("LDY zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xA4) == 3);
}

TEST_CASE("LDY zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xB4) == 4);
}

TEST_CASE("LDY absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xAC) == 4);
}

TEST_CASE("LDY absolute x (no page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xBC, 0x55, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("LDY absolute x (page cross) clock cycles", "[require]") {
    auto cpu = load_program({0xA2, 0xAA, 0xBC, 0x56, 0x11});
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 5);
}
