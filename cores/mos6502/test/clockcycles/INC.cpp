#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("INC zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE6) == 5);
}

TEST_CASE("INC zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xF6) == 6);
}

TEST_CASE("INC absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xEE) == 6);
}

TEST_CASE("INC absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xFE) == 7);
}
