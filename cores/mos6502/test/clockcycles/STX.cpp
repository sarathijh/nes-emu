#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("STX zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x86) == 3);
}

TEST_CASE("STX zero page y clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x96) == 4);
}

TEST_CASE("STX absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x8E) == 4);
}
