#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("CPX immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE0) == 2);
}

TEST_CASE("CPX zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xE4) == 3);
}

TEST_CASE("CPX absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xEC) == 4);
}
