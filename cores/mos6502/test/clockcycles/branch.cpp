#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("BPL (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load a negative value to set the negative status flag
        0xA9,
        0xFF,
        0x10,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BPL (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load a positive value to clear the negative status flag
        0xA9,
        0x00,
        // Branch forward half a page
        0x10,
        0x7F,
        // Branch forward another half page to force a page cross
        0x10,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BMI (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load a positive value to clear the negative status flag
        0xA9,
        0x00,
        0x30,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BMI (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load a negative value to set the negative status flag
        0xA9,
        0xFF,
        // Branch forward half a page
        0x30,
        0x7F,
        // Branch forward another half page to force a page cross
        0x30,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BVC (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Cause an overflow to set the overflow flag
        0x69,
        0x7F,
        0x69,
        0x7F,
        0x50,
    });

    cpu->exec_instruction();
    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BVC (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Clear overflow
        0xB8,
        // Branch forward half a page
        0x50,
        0x7F,
        // Branch forward another half page to force a page cross
        0x50,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BVS (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Clear overflow
        0xB8,
        0x70,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BVS (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Cause an overflow to set the overflow flag
        0x69,
        0x7F,
        0x69,
        0x7F,
        // Branch forward half a page
        0x70,
        0x7F,
        // Branch forward another half page to force a page cross
        0x70,
        0x7F,
    });

    cpu->exec_instruction();
    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BCC (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Set carry
        0x38,
        0x90,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BCC (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Clear carry
        0x18,
        // Branch forward half a page
        0x90,
        0x7F,
        // Branch forward another half page to force a page cross
        0x90,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BCS (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Clear carry
        0x18,
        0xB0,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BCS (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Set carry
        0x38,
        // Branch forward half a page
        0xB0,
        0x7F,
        // Branch forward another half page to force a page cross
        0xB0,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BNE (not taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load zero to set the zero flag
        0xA9,
        0x00,
        0xD0,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BNE (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load a non-zero value to clear the zero flag
        0xA9,
        0xFF,
        // Branch forward half a page
        0xD0,
        0x7F,
        // Branch forward another half page to force a page cross
        0xD0,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}

TEST_CASE("BEQ clock cycles", "[require]") {
    auto cpu = load_program({
        // Load a non-zero value to clear the zero flag
        0xA9,
        0xFF,
        0xF0,
    });

    cpu->exec_instruction();

    REQUIRE(cpu->exec_instruction() == 2);
}

TEST_CASE("BEQ (taken) clock cycles", "[require]") {
    auto cpu = load_program({
        // Load zero to set the zero flag
        0xA9,
        0x00,
        // Branch forward half a page
        0xF0,
        0x7F,
        // Branch forward another half page to force a page cross
        0xF0,
        0x7F,
    });

    cpu->exec_instruction();

    // Extra cycle for taking the branch
    REQUIRE(cpu->exec_instruction() == 3);

    // Extra cycle for page cross
    REQUIRE(cpu->exec_instruction() == 4);
}
