#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("CPY immediate clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC0) == 2);
}

TEST_CASE("CPY zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xC4) == 3);
}

TEST_CASE("CPY absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0xCC) == 4);
}
