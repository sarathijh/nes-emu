#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("JMP absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x4C) == 3);
}

TEST_CASE("JMP indirect clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x6C) == 5);
}
