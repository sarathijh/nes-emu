#include "../catch.hpp"
#include "../util/execute_opcode.hpp"

TEST_CASE("ASL accumulator clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x0A) == 2);
}

TEST_CASE("ASL zero page clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x06) == 5);
}

TEST_CASE("ASL zero page x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x16) == 6);
}

TEST_CASE("ASL absolute clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x0E) == 6);
}

TEST_CASE("ASL absolute x clock cycles", "[require]") {
    REQUIRE(execute_opcode(0x1E) == 7);
}
