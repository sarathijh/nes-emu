#include "execute_opcode.hpp"

#include "staticmemory.hpp"

int execute_opcode(uint8_t opcode) {
    auto memory = StaticMemory({opcode});
    auto cpu = Mos6502(&memory);

    return cpu.exec_instruction();
}

Mos6502 *load_program(const std::vector<uint8_t> &program) {
    return new Mos6502(new StaticMemory(program));
}
