#ifndef MOS6502_TEST_EXECUTE_OPCODE_HPP
#define MOS6502_TEST_EXECUTE_OPCODE_HPP

#include <cstdint>
#include <mos6502/mos6502.hpp>
#include <vector>

int execute_opcode(uint8_t opcode);
Mos6502 *load_program(const std::vector<uint8_t> &program);

#endif // MOS6502_TEST_EXECUTE_OPCODE_HPP
