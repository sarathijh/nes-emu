#ifndef MOS6502_TEST_STATICMEMORY_HPP
#define MOS6502_TEST_STATICMEMORY_HPP

#include <cstdint>
#include <mos6502/imemory.hpp>
#include <vector>

class StaticMemory : public IMemory {
private:
    std::vector<uint8_t> program;
    unsigned pc = 0;
    uint8_t _last_write = 0;

public:
    explicit StaticMemory(const std::vector<uint8_t> &program) {
        this->program = program;
    }

public:
    uint8_t last_write() { return _last_write; }

public:
    uint8_t read(uint16_t) override {
        auto val = do_read();
        return val;
    }

    uint8_t do_read() {
        if (pc >= program.size()) {
            return 0;
        }
        return program[pc++];
    }

    void write(uint16_t, uint8_t val) override { _last_write = val; }
};

#endif // MOS6502_TEST_STATICMEMORY_HPP
