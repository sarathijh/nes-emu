#include "../catch.hpp"
#include "../util/staticmemory.hpp"

#include <mos6502/mos6502.hpp>

void test_LDA(uint8_t a, uint8_t expected_flags);

TEST_CASE("LDA zero", "[require]") { test_LDA(0x00, 0b00010010); }

TEST_CASE("LDA positive", "[require]") {
    for (int val = 1; val <= 127; ++val) {
        SECTION(std::to_string(val)) { test_LDA(val, 0b00010000); }
    }
}

TEST_CASE("LDA negative", "[require]") {
    for (int val = 128; val <= 255; ++val) {
        SECTION(std::to_string(val)) { test_LDA(val, 0b10010000); }
    }
}

void test_LDA(uint8_t a, uint8_t expected_flags) {
    StaticMemory memory({
        // LDA
        0xA9,
        a,
        // STA $00 // Store A so we can capture the value
        0x85,
        0x00,
        // PHP // Push the status register to the stack so we can capture the
        // value
        0x08,
    });

    Mos6502 cpu(&memory);

    cpu.exec_instruction();
    cpu.exec_instruction();
    // The result of the add should be in A
    REQUIRE((int) memory.last_write() == (int) a);

    cpu.exec_instruction();
    // None of the status flags should be set
    REQUIRE((int) memory.last_write() == (int) expected_flags);
}
