#include "../catch.hpp"
#include "../util/staticmemory.hpp"

#include <mos6502/mos6502.hpp>

void test_LDX(uint8_t a, uint8_t expected_flags);

TEST_CASE("LDX zero", "[require]") { test_LDX(0x00, 0b00010010); }

TEST_CASE("LDX positive", "[require]") {
    for (int val = 1; val <= 127; ++val) {
        SECTION(std::to_string(val)) { test_LDX(val, 0b00010000); }
    }
}

TEST_CASE("LDX negative", "[require]") {
    for (int val = 128; val <= 255; ++val) {
        SECTION(std::to_string(val)) { test_LDX(val, 0b10010000); }
    }
}

void test_LDX(uint8_t a, uint8_t expected_flags) {
    StaticMemory memory({
        // LDX
        0xA2,
        a,
        // STX $00 // Store X so we can capture the value
        0x86,
        0x00,
        // PHP // Push the status register to the stack so we can capture the
        // value
        0x08,
    });

    Mos6502 cpu(&memory);

    cpu.exec_instruction();
    cpu.exec_instruction();
    // The result of the add should be in X
    REQUIRE((int) memory.last_write() == (int) a);

    cpu.exec_instruction();
    // None of the status flags should be set
    REQUIRE((int) memory.last_write() == (int) expected_flags);
}
