#include "../catch.hpp"
#include "../util/staticmemory.hpp"

#include <mos6502/mos6502.hpp>

void test_ADC(
    uint8_t a, uint8_t b, uint8_t expected_result, uint8_t expected_flags);

TEST_CASE("ADC [no status]", "[require]") {
    test_ADC(0x11, 0x22, 0x33, 0b00010000);
}

TEST_CASE("ADC [carry]", "[require]") {
    test_ADC(0xFF, 0x02, 0x01, 0b00010001);
}

TEST_CASE("ADC [carry & overflow]", "[require]") {
    test_ADC(0xAA, 0xAA, 0x54, 0b01010001);
}

TEST_CASE("ADC [carry & zero]", "[require]") {
    test_ADC(0xFF, 0x01, 0x00, 0b00010011);
}

TEST_CASE("ADC [overflow & negative]", "[require]") {
    test_ADC(0x7F, 0x01, 0x80, 0b11010000);
}

TEST_CASE("ADC [negative]", "[require]") {
    test_ADC(0xAA, 0x11, 0xBB, 0b10010000);
}

TEST_CASE("ADC [zero]", "[require]") { test_ADC(0x00, 0x00, 0x00, 0b00010010); }

void test_ADC(
    uint8_t a, uint8_t b, uint8_t expected_result, uint8_t expected_flags) {
    StaticMemory memory({
        // LDA
        0xA9,
        a,
        // ADC
        0x69,
        b,
        // STA $00 // Store A so we can capture the value
        0x85,
        0x00,
        // PHP // Push the status register to the stack so we can capture the
        // value
        0x08,
    });

    Mos6502 cpu(&memory);

    cpu.exec_instruction();
    cpu.exec_instruction();
    cpu.exec_instruction();
    // The result of the add should be in A
    REQUIRE((int) memory.last_write() == (int) expected_result);

    cpu.exec_instruction();
    // None of the status flags should be set
    REQUIRE((int) memory.last_write() == (int) expected_flags);
}
