#ifndef MOS6502_HPP
#define MOS6502_HPP

#include "imemory.hpp"

#include <cstdint>

/**
 * Emulates the MOS 6502 microprocessor.
 */
class Mos6502 {
private:
    union Flags {
        uint8_t bits;
        struct {
            bool carry : 1;
            bool zero : 1;
            bool interrupt : 1;
            bool decimal : 1;
            bool _ : 2;
            bool overflow : 1;
            bool negative : 1;
        };
    };

    enum class Interrupt { NMI, RESET, IRQ, BRK };

private:
    // Program Counter
    uint16_t PC = 0;
    // Accumulator, Index Registers, and Stack Pointer
    uint8_t A = 0, X = 0, Y = 0, S = 0;
    // Status Register
    Flags P;

    // These are set if the CPU has received an interrupt
    bool nmi = false, irq = false;

    IMemory *memory;

    int cycles = 0;

public:
    explicit Mos6502(IMemory *memory);

public:
    void invoke_nmi();
    void set_irq(bool on);
    void power_on();
    int exec_instruction();

private:
    void exec_instruction(uint8_t opcode);

private:
    void tick();

    // Memory Access
private:
    uint8_t read(uint16_t addr);
    uint8_t write(uint16_t addr, uint8_t val);
    uint16_t read_16(uint16_t addr);
    uint16_t read_16_no_page_cross(uint16_t addr);
    uint16_t read_16(uint16_t a, uint16_t b);
    uint8_t next();
    uint16_t next_16();

    // Stack
private:
    uint8_t pop();
    uint16_t pop_16();
    void push(uint8_t val);
    void push_16(uint16_t val);

private:
    void update_carry_overflow(uint8_t a, uint8_t b, uint16_t res);
    void update_negative_zero(uint8_t a);

    // Addressing Modes
private:
    uint16_t immediate();
    uint16_t absolute();
    uint16_t absolute_x(bool wr = false);
    uint16_t absolute_y(bool wr = false);
    uint8_t zero_page();
    uint8_t zero_page_x();
    uint8_t zero_page_y();
    uint16_t indirect();
    uint16_t indirect_x();
    uint16_t indirect_y(bool wr = false);
    uint16_t relative();

    template<typename T>
    uint16_t add_check_page_cross(uint16_t a, T b, bool wr = false) {
        uint16_t res = a + b;
        // If we crossed a page boundary, then add an extra cycle.
        // This is because the low byte of the operand will have the value added
        // to it and the address will be read with the high byte as-is. If the
        // add resulted in a carry (crossed page boundary), then we need to add
        // the carry to the high byte and read the correct address on the next
        // cycle.
        if (wr || (res & 0xFF00u) != (a & 0xFF00u)) {
            tick();
        }
        return res;
    }

    // Instructions
private:
    void ADC(uint16_t addr);
    void SBC(uint16_t addr);
    void add_accum(uint8_t val);
    void AND(uint16_t addr);
    void EOR(uint16_t addr);
    void ORA(uint16_t addr);
    void BIT(uint16_t addr);
    void ASL(uint16_t addr);
    void LSR(uint16_t addr);
    void ROL(uint16_t addr);
    void ROR(uint16_t addr);
    void ASL_A();
    void LSR_A();
    void ROL_A();
    void ROR_A();
    void DEC(uint16_t addr);
    void INC(uint16_t addr);
    void DEx(uint8_t &reg);
    void INx(uint8_t &reg);
    void LDx(uint8_t &reg, uint16_t addr);
    void STx(uint8_t val, uint16_t addr);
    void transfer(uint8_t &reg, uint8_t val);
    void TXS();
    void PLP();
    void PHP();
    void PLA();
    void PHA();
    void CMP(uint8_t val, uint16_t addr);
    void branch(bool take_branch);
    void JSR();
    void RTS();
    void RTI();
    void interrupt(Interrupt kind);
};

#endif // MOS6502_HPP
