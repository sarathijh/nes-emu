#ifndef MOS6502_IMEMORY_HPP
#define MOS6502_IMEMORY_HPP

#include <cstdint>

class IMemory {
public:
    virtual uint8_t read(uint16_t addr) = 0;

    virtual void write(uint16_t addr, uint8_t val) = 0;
};

#endif // MOS6502_IMEMORY_HPP
