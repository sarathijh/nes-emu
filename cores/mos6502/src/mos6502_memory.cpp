#include "../include/mos6502/mos6502.hpp"

uint8_t Mos6502::read(uint16_t addr) { return memory->read(addr); }

uint8_t Mos6502::write(uint16_t addr, uint8_t val) {
    memory->write(addr, val);
    return val;
}

uint16_t Mos6502::read_16(uint16_t addr) {
    return read_16(addr, (uint16_t)(addr + 1u));
}

uint16_t Mos6502::read_16_no_page_cross(uint16_t addr) {
    return read_16(addr, (uint16_t)((addr + 1u) & 0xFFu));
}

uint16_t Mos6502::read_16(uint16_t a, uint16_t b) {
    return (uint16_t)(read(a) | (unsigned) (read(b) << 8u));
}

uint8_t Mos6502::next() { return read(PC++); }

uint16_t Mos6502::next_16() {
    uint16_t op = read_16(PC);
    PC += 2u;
    return op;
}
