#include "../include/mos6502/mos6502.hpp"

uint8_t Mos6502::pop() { return read((uint16_t)(0x0100u + (++S))); }

uint16_t Mos6502::pop_16() {
    return (uint16_t)(pop() | (unsigned) (pop() << 8u));
}

void Mos6502::push(uint8_t val) { write((uint16_t)(0x0100u + (S--)), val); }

void Mos6502::push_16(uint16_t val) {
    push((uint8_t)(val >> 8u));
    push((uint8_t)(val & 0xFFu));
}
