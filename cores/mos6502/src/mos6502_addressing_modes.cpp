#include "../include/mos6502/mos6502.hpp"

uint16_t Mos6502::immediate() { return PC++; }

uint16_t Mos6502::absolute() {
    tick();
    tick();
    return next_16();
}

uint16_t Mos6502::absolute_x(bool wr) {
    return add_check_page_cross(absolute(), X, wr);
}

uint16_t Mos6502::absolute_y(bool wr) {
    return add_check_page_cross(absolute(), Y, wr);
}

uint8_t Mos6502::zero_page() {
    tick();
    return next();
}

uint8_t Mos6502::zero_page_x() {
    tick();
    return zero_page() + X;
}

uint8_t Mos6502::zero_page_y() {
    tick();
    return zero_page() + Y;
}

uint16_t Mos6502::indirect() {
    tick();
    tick();
    tick();
    return read_16_no_page_cross(next_16());
}

uint16_t Mos6502::indirect_x() {
    tick();
    tick();
    tick();
    tick();
    return read_16_no_page_cross(next() + X);
}

uint16_t Mos6502::indirect_y(bool wr) {
    tick();
    tick();
    tick();
    return add_check_page_cross(read_16_no_page_cross(next()), Y, wr);
}

uint16_t Mos6502::relative() {
    int8_t offset = next();
    return PC + offset;
}
