#include "../include/mos6502/mos6502.hpp"

Mos6502::Mos6502(IMemory *memory) : P() { this->memory = memory; }

void Mos6502::invoke_nmi() { nmi = true; }

void Mos6502::set_irq(bool on) { irq = on; }

void Mos6502::power_on() {
    cycles = 0;

    PC = 0;
    S = 0xFF;

    A = X = Y = 0;
    P.bits = 0x34;

    nmi = irq = false;

    interrupt(Interrupt::RESET);
}

void Mos6502::interrupt(Interrupt kind) {
    if (kind != Interrupt::RESET) {
        push_16(PC);
        push((uint8_t)(P.bits | (unsigned) ((kind == Interrupt::BRK) << 4u)));
    } else {
        S -= 3;
        tick();
        tick();
        tick();
    }

    P.interrupt = true;

    static constexpr uint16_t vectors[] = { 0xFFFA, 0xFFFC, 0xFFFE, 0xFFFE };
    PC = read_16(vectors[(int) kind]);

    if (kind == Interrupt::BRK) {
        tick();
        tick();
        tick();
        tick();
        tick();
    }

    if (kind == Interrupt::NMI) {
        nmi = false;
    }
}

int Mos6502::exec_instruction() {
    cycles = 0;

    if (nmi) {
        interrupt(Interrupt::NMI);
    }
    if (irq && !P.interrupt) {
        interrupt(Interrupt::IRQ);
    }

    exec_instruction(next());

    return cycles;
}

void Mos6502::tick() { cycles += 1; }

void Mos6502::update_carry_overflow(uint8_t a, uint8_t b, uint16_t res) {
    P.carry = res > 0xFF;
    P.overflow = (~(unsigned) (a ^ b) & (unsigned) (a ^ res) & 0x80u) != 0;
}

void Mos6502::update_negative_zero(uint8_t a) {
    P.negative = (a & 0x80u) != 0;
    P.zero = (a == 0);
}

void Mos6502::exec_instruction(uint8_t opcode) {
    tick(); // fetch opcode
    tick(); // interpret opcode and fetch first operand

    switch (opcode) {
    // ADC - ADd with Carry
    case 0x69: ADC(immediate()); break;
    case 0x65: ADC(zero_page()); break;
    case 0x75: ADC(zero_page_x()); break;
    case 0x6D: ADC(absolute()); break;
    case 0x7D: ADC(absolute_x()); break;
    case 0x79: ADC(absolute_y()); break;
    case 0x61: ADC(indirect_x()); break;
    case 0x71:
        ADC(indirect_y());
        break;

        // SBC - SuBtract with Carry
    case 0xE9: SBC(immediate()); break;
    case 0xE5: SBC(zero_page()); break;
    case 0xF5: SBC(zero_page_x()); break;
    case 0xED: SBC(absolute()); break;
    case 0xFD: SBC(absolute_x()); break;
    case 0xF9: SBC(absolute_y()); break;
    case 0xE1: SBC(indirect_x()); break;
    case 0xF1:
        SBC(indirect_y());
        break;

        // AND - bitwise AND with accum
    case 0x29: AND(immediate()); break;
    case 0x25: AND(zero_page()); break;
    case 0x35: AND(zero_page_x()); break;
    case 0x2D: AND(absolute()); break;
    case 0x3D: AND(absolute_x()); break;
    case 0x39: AND(absolute_y()); break;
    case 0x21: AND(indirect_x()); break;
    case 0x31:
        AND(indirect_y());
        break;

        // EOR - bitwise Exclusive OR
    case 0x49: EOR(immediate()); break;
    case 0x45: EOR(zero_page()); break;
    case 0x55: EOR(zero_page_x()); break;
    case 0x4D: EOR(absolute()); break;
    case 0x5D: EOR(absolute_x()); break;
    case 0x59: EOR(absolute_y()); break;
    case 0x41: EOR(indirect_x()); break;
    case 0x51:
        EOR(indirect_y());
        break;

        // ORA - bitwise OR with Accum
    case 0x09: ORA(immediate()); break;
    case 0x05: ORA(zero_page()); break;
    case 0x15: ORA(zero_page_x()); break;
    case 0x0D: ORA(absolute()); break;
    case 0x1D: ORA(absolute_x()); break;
    case 0x19: ORA(absolute_y()); break;
    case 0x01: ORA(indirect_x()); break;
    case 0x11:
        ORA(indirect_y());
        break;

        // BIT - test BITs
    case 0x24: BIT(zero_page()); break;
    case 0x2C:
        BIT(absolute());
        break;

        // ASL - Arithmetic Shift Left
    case 0x0A: ASL_A(); break;
    case 0x06: ASL(zero_page()); break;
    case 0x16: ASL(zero_page_x()); break;
    case 0x0E: ASL(absolute()); break;
    case 0x1E:
        ASL(absolute_x(true));
        break;

        // LSR - Logical Shift Right
    case 0x4A: LSR_A(); break;
    case 0x46: LSR(zero_page()); break;
    case 0x56: LSR(zero_page_x()); break;
    case 0x4E: LSR(absolute()); break;
    case 0x5E:
        LSR(absolute_x(true));
        break;

        // ROL - ROtate Left
    case 0x2A: ROL_A(); break;
    case 0x26: ROL(zero_page()); break;
    case 0x36: ROL(zero_page_x()); break;
    case 0x2E: ROL(absolute()); break;
    case 0x3E:
        ROL(absolute_x(true));
        break;

        // ROR - ROtate Right
    case 0x6A: ROR_A(); break;
    case 0x66: ROR(zero_page()); break;
    case 0x76: ROR(zero_page_x()); break;
    case 0x6E: ROR(absolute()); break;
    case 0x7E:
        ROR(absolute_x(true));
        break;

        // DEC - DECrement memory
    case 0xC6: DEC(zero_page()); break;
    case 0xD6: DEC(zero_page_x()); break;
    case 0xCE: DEC(absolute()); break;
    case 0xDE:
        DEC(absolute_x(true));
        break;

        // INC - INCrement memory
    case 0xE6: INC(zero_page()); break;
    case 0xF6: INC(zero_page_x()); break;
    case 0xEE: INC(absolute()); break;
    case 0xFE:
        INC(absolute_x(true));
        break;

        // LDA - LoaD Accumulator
    case 0xA9: LDx(A, immediate()); break;
    case 0xA5: LDx(A, zero_page()); break;
    case 0xB5: LDx(A, zero_page_x()); break;
    case 0xAD: LDx(A, absolute()); break;
    case 0xBD: LDx(A, absolute_x()); break;
    case 0xB9: LDx(A, absolute_y()); break;
    case 0xA1: LDx(A, indirect_x()); break;
    case 0xB1:
        LDx(A, indirect_y());
        break;

        // STA - STore Accumulator
    case 0x85: STx(A, zero_page()); break;
    case 0x95: STx(A, zero_page_x()); break;
    case 0x8D: STx(A, absolute()); break;
    case 0x9D: STx(A, absolute_x(true)); break;
    case 0x99: STx(A, absolute_y(true)); break;
    case 0x81: STx(A, indirect_x()); break;
    case 0x91:
        STx(A, indirect_y(true));
        break;

        // LDX - LoaD X register
    case 0xA2: LDx(X, immediate()); break;
    case 0xA6: LDx(X, zero_page()); break;
    case 0xB6: LDx(X, zero_page_y()); break;
    case 0xAE: LDx(X, absolute()); break;
    case 0xBE:
        LDx(X, absolute_y());
        break;

        // LDY - LoaD Y register
    case 0xA0: LDx(Y, immediate()); break;
    case 0xA4: LDx(Y, zero_page()); break;
    case 0xB4: LDx(Y, zero_page_x()); break;
    case 0xAC: LDx(Y, absolute()); break;
    case 0xBC:
        LDx(Y, absolute_x());
        break;

        // STX - STore X register
    case 0x86: STx(X, zero_page()); break;
    case 0x96: STx(X, zero_page_y()); break;
    case 0x8E:
        STx(X, absolute());
        break;

        // STY - STore Y register
    case 0x84: STx(Y, zero_page()); break;
    case 0x94: STx(Y, zero_page_x()); break;
    case 0x8C:
        STx(Y, absolute());
        break;

        // Register Instructions
    case 0xAA: transfer(X, A); break;
    case 0x8A: transfer(A, X); break;
    case 0xCA: DEx(X); break;
    case 0xE8: INx(X); break;
    case 0xA8: transfer(Y, A); break;
    case 0x98: transfer(A, Y); break;
    case 0x88: DEx(Y); break;
    case 0xC8:
        INx(Y);
        break;

        // Stack Instructions
    case 0x9A: TXS(); break;
    case 0xBA: transfer(X, S); break;
    case 0x48: PHA(); break;
    case 0x68: PLA(); break;
    case 0x08: PHP(); break;
    case 0x28:
        PLP();
        break;

        // CMP - CoMPare accumulator
    case 0xC9: CMP(A, immediate()); break;
    case 0xC5: CMP(A, zero_page()); break;
    case 0xD5: CMP(A, zero_page_x()); break;
    case 0xCD: CMP(A, absolute()); break;
    case 0xDD: CMP(A, absolute_x()); break;
    case 0xD9: CMP(A, absolute_y()); break;
    case 0xC1: CMP(A, indirect_x()); break;
    case 0xD1:
        CMP(A, indirect_y());
        break;

        // CPX - ComPare X register
    case 0xE0: CMP(X, immediate()); break;
    case 0xE4: CMP(X, zero_page()); break;
    case 0xEC:
        CMP(X, absolute());
        break;

        // CPY - ComPare Y register
    case 0xC0: CMP(Y, immediate()); break;
    case 0xC4: CMP(Y, zero_page()); break;
    case 0xCC:
        CMP(Y, absolute());
        break;

        // Branch Instructions
    case 0x10: branch(P.negative == 0); break;
    case 0x30: branch(P.negative == 1); break;
    case 0x50: branch(P.overflow == 0); break;
    case 0x70: branch(P.overflow == 1); break;
    case 0x90: branch(P.carry == 0); break;
    case 0xB0: branch(P.carry == 1); break;
    case 0xD0: branch(P.zero == 0); break;
    case 0xF0:
        branch(P.zero == 1);
        break;

        // JMP - JuMP (Absolute)
    case 0x4C: {
        tick();
        PC = next_16();
        break;
    }
        // JMP - JuMP (Indirect)
    case 0x6C:
        PC = indirect();
        break;
        // JSR - Jump to SubRoutine
    case 0x20:
        JSR();
        break;
        // RTI - ReTurn from Interrupt
    case 0x40:
        RTI();
        break;
        // RTS - ReTurn from Subroutine
    case 0x60:
        RTS();
        break;

        // Flag Instructions - Processor Status
    case 0x18: P.carry = false; break;
    case 0x38: P.carry = true; break;
    case 0x58: P.interrupt = false; break;
    case 0x78: P.interrupt = true; break;
    case 0xB8: P.overflow = false; break;
    case 0xD8: P.decimal = false; break;
    case 0xF8:
        P.decimal = true;
        break;

        // BRK - BReaK
    case 0x00:
        interrupt(Interrupt::BRK);
        break;

        // NOP - No OPeration
    case 0xEA: break;

    default: break;
    }
}
