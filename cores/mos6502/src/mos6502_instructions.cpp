#include "../include/mos6502/mos6502.hpp"

void Mos6502::ADC(uint16_t addr) { add_accum(read(addr)); }

void Mos6502::SBC(uint16_t addr) { add_accum((uint8_t)(read(addr) ^ 0xFFu)); }

void Mos6502::add_accum(uint8_t val) {
    uint16_t res = A + val + P.carry;
    update_carry_overflow(A, val, res);
    update_negative_zero((uint8_t) res);
    A = (uint8_t) res;
}

void Mos6502::AND(uint16_t addr) { update_negative_zero(A &= read(addr)); }

void Mos6502::EOR(uint16_t addr) { update_negative_zero(A ^= read(addr)); }

void Mos6502::ORA(uint16_t addr) { update_negative_zero(A |= read(addr)); }

void Mos6502::BIT(uint16_t addr) {
    uint8_t val = read(addr);

    P.zero = ((A & val) == 0);
    P.negative = (val & 0x80u) != 0;
    P.overflow = (val & 0x40u) != 0;
}

void Mos6502::ASL(uint16_t addr) {
    tick();
    tick();
    uint8_t v = read(addr);
    update_negative_zero(write(addr, v << 1u));
    P.carry = (v & 0x80u) != 0;
}

void Mos6502::LSR(uint16_t addr) {
    tick();
    tick();
    uint8_t v = read(addr);
    update_negative_zero(write(addr, v >> 1u));
    P.carry = (v & 0x01u) != 0;
}

void Mos6502::ROL(uint16_t addr) {
    tick();
    tick();
    uint8_t v = read(addr);
    update_negative_zero(
        write(addr, (uint8_t)((unsigned) (v << 1u) | P.carry)));
    P.carry = (v & 0x80u) != 0;
}

void Mos6502::ROR(uint16_t addr) {
    tick();
    tick();
    uint8_t v = read(addr);
    update_negative_zero(write(
        addr, (uint8_t)((unsigned) (P.carry << 7u) | (unsigned) (v >> 1u))));
    P.carry = (v & 0x01u) != 0;
}

void Mos6502::ASL_A() {
    P.carry = (A & 0x80u) != 0;
    update_negative_zero(A <<= 1u);
}

void Mos6502::LSR_A() {
    P.carry = (A & 0x01u) != 0;
    update_negative_zero(A >>= 1u);
}

void Mos6502::ROL_A() {
    auto res = (uint8_t)((unsigned) (A << 1u) | P.carry);
    P.carry = (A & 0x80u) != 0;
    update_negative_zero(A = res);
}

void Mos6502::ROR_A() {
    auto res = (uint8_t)((unsigned) (P.carry << 7u) | (unsigned) (A >> 1u));
    P.carry = (A & 0x01u) != 0;
    update_negative_zero(A = res);
}

void Mos6502::DEC(uint16_t addr) {
    tick();
    tick();
    uint8_t val = read(addr);
    update_negative_zero(write(addr, --val));
}

void Mos6502::INC(uint16_t addr) {
    tick();
    tick();
    uint8_t val = read(addr);
    update_negative_zero(write(addr, ++val));
}

void Mos6502::DEx(uint8_t &reg) { update_negative_zero(--reg); }

void Mos6502::INx(uint8_t &reg) { update_negative_zero(++reg); }

void Mos6502::LDx(uint8_t &reg, uint16_t addr) {
    update_negative_zero(reg = read(addr));
}

void Mos6502::STx(uint8_t val, uint16_t addr) { write(addr, val); }

void Mos6502::transfer(uint8_t &reg, uint8_t val) {
    update_negative_zero(reg = val);
}

void Mos6502::TXS() { S = X; }

void Mos6502::PLP() {
    tick();
    tick();
    P.bits = pop();
}

void Mos6502::PHP() {
    tick();
    push((uint8_t)(P.bits | (1u << 4u)));
}

void Mos6502::PLA() {
    tick();
    tick();
    update_negative_zero(A = pop());
}

void Mos6502::PHA() {
    tick();
    push(A);
}

void Mos6502::CMP(uint8_t val, uint16_t addr) {
    uint8_t b = read(addr);
    update_negative_zero(val - b);
    P.carry = (val >= b);
}

void Mos6502::branch(bool take_branch) {
    uint16_t addr = relative();
    if (take_branch) {
        // add 1 to cycles if branch occurs on same page
        // add 2 to cycles if branch occurs to different page
        tick();
        if ((addr & 0xFF00u) != ((PC + 1u) & 0xFF00u)) {
            tick();
        }
        PC = addr;
    }
}

void Mos6502::JSR() {
    tick();
    tick();
    push_16((uint16_t)(PC + 1u));
    PC = absolute();
}

void Mos6502::RTS() {
    tick();
    tick();
    tick();
    tick();
    PC = (uint16_t)(pop_16() + 1u);
}

void Mos6502::RTI() {
    tick();
    tick();
    PLP();
    PC = pop_16();
}
