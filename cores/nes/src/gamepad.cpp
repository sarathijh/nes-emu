#include "gamepad.hpp"

uint8_t Gamepad::read_button(unsigned port) {
    // In the NES and Famicom, the top three (or five) bits are not driven, and
    // so retain the bits of the previous byte on the bus. Usually this is the
    // most significant byte of the address of the controller port—0x40. Certain
    // games (such as Paperboy) rely on this behavior and require that reads
    // from the controller ports return exactly $40 or $41 as appropriate.

    // While the strobe is high, the shift registers in the controllers are
    // continuously reloaded from the button states, and reading will keep
    // returning the current state of the first button (A).
    if (strobe) {
        return 0x40u | (buttons[port] & 1u);
    }

    // Read the next button state from the shift register, then shift to the
    // next button. After all the button states have been read, all subsequent
    // reads will return 1 on official Nintendo brand controllers; therefore,
    // 1 is shifted into the high bit.
    uint8_t button = 0x40u | (shift_regs[port] & 1u);

    shift_regs[port] >>= 1u;
    shift_regs[port] |= 0b10000000u;

    return button;
}

void Gamepad::write_strobe(bool state) {
    // When the strobe goes from high to low, reload the shift registers with
    // the current button states.
    if (strobe && !state) {
        memcpy(shift_regs, buttons, 2);
    }
    strobe = state;
}

void Gamepad::set_button_state(unsigned port, unsigned button, unsigned state) {
    buttons[port] ^= (-state ^ buttons[port]) & (1u << button);
}
