#include "../include/nes/nes.hpp"

#include "apu/apu.hpp"
#include "cart.hpp"
#include "gamepad.hpp"
#include "mappedmemory.hpp"
#include "ppu.hpp"

#include <mos6502/mos6502.hpp>

const int cycles_per_frame = 29780;

std::string replace_ext(const std::string &filename, const std::string &ext) {
    return filename.substr(0, filename.find_last_of('.')) + "." + ext;
}

NES::NES(const std::string &rom_file_path) {
    save_file_path = replace_ext(rom_file_path, "sav");

    cart = new Cart();
    cart->irq_handler = [this](bool on) { cpu->set_irq(on); };
    cart->load_rom(rom_file_path);

    gamepad = new Gamepad();

    ppu = new PPU(cart);
    ppu->nmi_handler = [this]() { cpu->invoke_nmi(); };
    ppu->post_handler = [this](unsigned *pixels) { frame_handler(pixels); };
    ppu->on_scanline_done = [this]() { cart->on_scanline_done(); };

    apu = new APU();
    apu->irq_handler = [&]() { cpu->set_irq(true); };
    apu->sample_handler = [&](uint8_t sample) { audio_handler(sample); };

    mapped_memory = new MappedMemory(ppu, apu, cart, gamepad);
    cpu = new Mos6502(mapped_memory);

    load();
}

NES::~NES() { save(); }

void NES::power_on() { cpu->power_on(); }

void NES::execute_frame() {
    int cycles_remaining = cycles_per_frame;
    while (cycles_remaining > 0) {
        int cycles = cpu->exec_instruction();

        for (int i = 0; i < cycles; ++i) {
            // 3 PPU ticks per CPU tick
            ppu->tick();
            ppu->tick();
            ppu->tick();
            // 2 APU ticks per CPU tick
            apu->tick();
            apu->tick();
        }

        cycles_remaining -= cycles;
    }
}

void NES::set_button(int button_index, bool pressed) {
    gamepad->set_button_state(0, button_index, pressed);
}

void NES::save() {
    if (cart->has_battery_backed_ram()) {
        std::ofstream file(save_file_path.data(), std::ios::binary);
        file.write((char *) cart->ram(), cart->ram_size());
        file.close();
    }
}

void NES::load() {
    if (cart->has_battery_backed_ram()) {
        std::ifstream file(save_file_path.data(), std::ios::binary);
        file.read((char *) cart->ram(), cart->ram_size());
        file.close();
    }
}
