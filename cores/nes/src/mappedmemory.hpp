#ifndef NESEMU_MAPPEDMEMORY_HPP
#define NESEMU_MAPPEDMEMORY_HPP

#include "gamepad.hpp"

#include <mos6502/imemory.hpp>

class MappedMemory : public IMemory {
private:
    IMemory *ppu;
    IMemory *apu;
    IMemory *cart;
    Gamepad *gamepad;

    // 2KB of RAM
    uint8_t ram[0x800] {};

public:
    MappedMemory(
        IMemory *ppu_memory,
        IMemory *apu_memory,
        IMemory *cart_memory,
        Gamepad *gamepad);

public:
    uint8_t read(uint16_t addr) override;

    void write(uint16_t addr, uint8_t val) override;

    /**
     * Emulates the Direct Memory Access component of the NES to write a
     * 256-byte block of Object Attribute Memory data to VRAM independent
     * of the CPU.
     *
     * @param val
     */
    void write_oam_dma(uint8_t val);
};

#endif // NESEMU_MAPPEDMEMORY_HPP
