#include "cart.hpp"

Cart::~Cart() {
    delete mapper;
    delete rom;
}

uint8_t Cart::read(uint16_t addr) { return mapper->read(addr); }

void Cart::write(uint16_t addr, uint8_t val) { mapper->write(addr, val); }

uint8_t Cart::chr_read(uint16_t addr) { return mapper->chr_read(addr); }

void Cart::chr_write(uint16_t addr, uint8_t val) {
    mapper->chr_write(addr, val);
}

Mirroring Cart::mirroring() { return mapper->mirroring; }

void Cart::on_scanline_done() { mapper->on_scanline_done(); }

bool Cart::has_battery_backed_ram() { return mapper->has_battery_backed_ram(); }

uint8_t *Cart::ram() { return mapper->ram(); }

Mapper *mapper_for_type(unsigned mapper_type, uint8_t *rom);

void Cart::load_rom(const std::string &filename) {
    std::ifstream file(filename, std::ios::binary);

    file.seekg(0, std::ios::end);
    size_t size = file.tellg();
    file.seekg(0);

    rom = new uint8_t[size];
    file.read((char *) rom, size);

    mapper = mapper_for_type((rom[7] & 0xF0u) | (unsigned) (rom[6] >> 4u), rom);
    mapper->irq_handler = [this](bool on) { irq_handler(on); };
}

Mapper *mapper_for_type(unsigned mapper_type, uint8_t *rom) {
    switch (mapper_type) {
    case 0: return new NROM(rom);
    case 1: return new MMC1(rom);
    case 2: return new UxROM(rom);
    case 4: return new MMC3(rom);
    default: {
        std::cerr << "Invalid mapper type: " << mapper_type << std::endl;
        return nullptr;
    }
    }
}
