#include "mappedmemory.hpp"

MappedMemory::MappedMemory(
    IMemory *ppu_memory,
    IMemory *apu_memory,
    IMemory *cart_memory,
    Gamepad *gamepad) {
    ppu = ppu_memory;
    apu = apu_memory;
    cart = cart_memory;
    this->gamepad = gamepad;

    memset(ram, 0, 0x800);
}

uint8_t MappedMemory::read(uint16_t addr) {
    if (addr <= 0x1FFF) {
        return ram[addr % 0x800];
    } else if (addr <= 0x3FFF) {
        return ppu->read(addr % 8);
    } else if (addr <= 0x4013) {
        return apu->read(addr);
    } else if (addr == 0x4014) {
        /* Write only */
    } else if (addr == 0x4015) {
        return apu->read(addr);
    } else if (addr == 0x4016) {
        return gamepad->read_button(0);
    } else if (addr == 0x4017) {
        return gamepad->read_button(1);
    } else if (addr <= 0xFFFF) {
        return cart->read(addr);
    }
    return 0;
}

void MappedMemory::write(uint16_t addr, uint8_t val) {
    if (addr <= 0x1FFF) {
        ram[addr % 0x800] = val;
    } else if (addr <= 0x3FFF) {
        ppu->write(addr % 8, val);
    } else if (addr <= 0x4013) {
        apu->write(addr, val);
    } else if (addr == 0x4014) {
        write_oam_dma(val);
    } else if (addr == 0x4015) {
        apu->write(addr, val);
    } else if (addr == 0x4016) {
        gamepad->write_strobe(val);
    } else if (addr == 0x4017) {
        apu->write(addr, val);
    } else if (addr <= 0xFFFF) {
        cart->write(addr, val);
    }
}

void MappedMemory::write_oam_dma(uint8_t val) {
    for (int i = 0; i <= 0xFF; ++i) {
        write(0x2004, read(val * 0x100 + i));
    }
}
