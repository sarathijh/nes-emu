#ifndef NROM_HPP
#define NROM_HPP

#include "../mapper.hpp"

class NROM : public Mapper {
public:
    using Mapper::Mapper;

public:
    uint8_t read(uint16_t addr) override;
    void write(uint16_t, uint8_t) override;
    uint8_t chr_read(uint16_t addr) override;
    void chr_write(uint16_t, uint8_t) override;
};

#endif // NROM_HPP
