#include "NROM.hpp"

uint8_t NROM::read(uint16_t addr) {
    if (addr < 0x8000) {
        return prg_ram[addr - 0x6000];
    } else {
        return prg[(addr - 0x8000) % prg_size];
    }
}

void NROM::write(uint16_t, uint8_t) {}

uint8_t NROM::chr_read(uint16_t addr) { return chr[addr]; }

void NROM::chr_write(uint16_t, uint8_t) {}
