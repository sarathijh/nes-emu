#include "MMC1.hpp"

uint8_t MMC1::read(uint16_t addr) {
    if (addr <= 0x7FFF) {
        return prg_ram[addr - 0x6000];
    } else {
        auto mode = (unsigned) (regs[0] >> 2u) & 0b11u;
        auto bank_select = regs[3] & 0xFu;

        switch (mode) {
        case 0:
        case 1: // Switch 32 KB at $8000, ignoring low bit of bank number.
            return prg
                [((bank_select >> 1u) * 0x8000 + (addr - 0x8000)) % prg_size];
        case 2: // Fix first bank at $8000 and switch 16 KB bank at $C000.
            if (addr <= 0xBFFF) {
                return prg[(addr - 0x8000) % prg_size];
            } else if (addr <= 0xFFFF) {
                return prg[(bank_select * 0x4000 + (addr - 0xC000)) % prg_size];
            }
            break;
        case 3: // Fix last bank at $C000 and switch 16 KB bank at $8000).
            if (addr <= 0xBFFF) {
                return prg[(bank_select * 0x4000 + (addr - 0x8000)) % prg_size];
            } else if (addr <= 0xFFFF) {
                return prg[((prg_size - 0x4000) + (addr - 0xC000)) % prg_size];
            }
            break;
        default: break;
        }
    }
    return 0;
}

void MMC1::write(uint16_t addr, uint8_t val) {
    if (addr <= 0x7FFF) {
        prg_ram[addr - 0x6000] = val;
    } else {
        // Writing a value with bit 7 set ($80 through $FF) to any address
        // in $8000-$FFFF clears the shift register to its initial state.
        if (val & 0x80u) {
            write_bit = shift_reg = 0;
            regs[0] |= 0x0Cu;
            update_mirroring();
        } else {
            // To change a register's value, the CPU writes five times with
            // bit 7 clear and a bit of the desired value in bit 0. On the
            // first four writes, the MMC1 shifts bit 0 into a shift
            // register.
            shift_reg |= (unsigned) (val & 1u) << write_bit;
            if (++write_bit == 5) {
                // On the fifth write, the MMC1 copies bit 0 and the shift
                // register contents into an internal register selected by
                // bits 14 and 13 of the address, and then it clears the
                // shift register.
                regs[(unsigned) (addr >> 13u) & 0b11u] = shift_reg;
                write_bit = shift_reg = 0;
                update_mirroring();
            }
        }
    }
}

uint8_t MMC1::chr_read(uint16_t addr) {
    auto mode = (unsigned) (regs[0] >> 4u) & 1u;
    switch (mode) {
    case 0: // Switch 8 KB at a time.
        return chr[((regs[1] >> 1u) * 0x2000 + addr) % chr_size];
    case 1: // Switch two separate 4 KB banks.
        if (addr <= 0x0FFF) {
            return chr[(regs[1] * 0x1000 + (addr - 0x0000)) % chr_size];
        } else if (addr <= 0x1FFF) {
            return chr[(regs[2] * 0x1000 + (addr - 0x1000)) % chr_size];
        }
    default: break;
    }
    return 0;
}

void MMC1::chr_write(uint16_t addr, uint8_t val) { chr[addr] = val; }

void MMC1::update_mirroring() {
    switch (regs[0] & 0b11u) {
        // case 0: break; // one-screen, lower bank
        // case 1: break; // one-screen, upper bank
    case 2: mirroring = Mirroring::VERTICAL; break;
    case 3: mirroring = Mirroring::HORIZONTAL; break;
    default: break;
    }
}
