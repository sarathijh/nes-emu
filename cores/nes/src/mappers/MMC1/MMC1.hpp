#ifndef MMC1_HPP
#define MMC1_HPP

#include "../mapper.hpp"

class MMC1 : public Mapper {
private:
    unsigned write_bit = 0;
    uint8_t regs[4] {0x0C, 0, 0, 0};
    uint8_t shift_reg = 0;

public:
    using Mapper::Mapper;

public:
    uint8_t read(uint16_t addr) override;
    void write(uint16_t addr, uint8_t val) override;
    uint8_t chr_read(uint16_t addr) override;
    void chr_write(uint16_t addr, uint8_t val) override;

private:
    void update_mirroring();
};

#endif // MMC1_HPP
