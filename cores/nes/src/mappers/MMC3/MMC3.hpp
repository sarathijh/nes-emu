#ifndef MMC3_HPP
#define MMC3_HPP

#include "../mapper.hpp"

class MMC3 : public Mapper {
private:
    uint8_t bank_select = 0;
    uint8_t banks[8] {0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t irq_latch = 0;
    uint8_t irq_counter = 0;
    bool irq_enabled = false;

public:
    using Mapper::Mapper;

public:
    uint8_t read(uint16_t addr) override;
    void write(uint16_t addr, uint8_t val) override;
    uint8_t chr_read(uint16_t addr) override;
    void chr_write(uint16_t addr, uint8_t val) override;

public:
    void on_scanline_done() override;
};

#endif // MMC3_HPP
