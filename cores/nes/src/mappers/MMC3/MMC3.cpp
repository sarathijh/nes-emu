#include "MMC3.hpp"

uint8_t MMC3::read(uint16_t addr) {
    uint8_t val = 0;
    if (addr <= 0x7FFF) {
        val = prg_ram[addr - 0x6000];
    } else if (addr <= 0x9FFF) {
        switch ((unsigned) (bank_select >> 6u) & 1u) {
        case 0:
            val = prg[(banks[6] * 0x2000 + (addr - 0x8000)) % prg_size];
            break;
        case 1:
            val = prg[((prg_size - 0x4000) + (addr - 0x8000)) % prg_size];
            break;
        }
    } else if (addr <= 0xBFFF) {
        val = prg[(banks[7] * 0x2000 + (addr - 0xA000)) % prg_size];
    } else if (addr <= 0xDFFF) {
        switch ((unsigned) (bank_select >> 6u) & 1u) {
        case 0:
            val = prg[((prg_size - 0x4000) + (addr - 0xC000)) % prg_size];
            break;
        case 1:
            val = prg[(banks[6] * 0x2000 + (addr - 0xC000)) % prg_size];
            break;
        }
    } else if (addr <= 0xFFFF) {
        val = prg[((prg_size - 0x2000) + (addr - 0xE000)) % prg_size];
    }
    return val;
}

void MMC3::write(uint16_t addr, uint8_t val) {
    if (addr <= 0x7FFF) {
        prg_ram[addr - 0x6000] = val;
    } else {
        bool even = (addr % 2 == 0);
        if (addr <= 0x9FFF) {
            if (even) {
                bank_select = val;
            } else {
                banks[bank_select & 0b111u] = val;
            }
        } else if (addr <= 0xBFFF && even) {
            mirroring = (Mirroring)(val & 1u);
        } else if (addr <= 0xDFFF) {
            if (even) {
                irq_latch = val;
            } else {
                irq_counter = 0;
            }
        } else if (addr <= 0xFFFF) {
            if (even) {
                irq_handler(irq_enabled = false);
            } else {
                irq_enabled = true;
            }
        }
    }
}

uint8_t MMC3::chr_read(uint16_t addr) {
    uint8_t val = 0;
    if ((unsigned) (bank_select >> 7u) & 1u) {
        if (addr <= 0x03FF) {
            val = chr[((banks[2]) * 0x400 + (addr)) % chr_size];
        } else if (addr <= 0x07FF) {
            val = chr[((banks[3]) * 0x400 + (addr - 0x400)) % chr_size];
        } else if (addr <= 0x0BFF) {
            val = chr[((banks[4]) * 0x400 + (addr - 0x800)) % chr_size];
        } else if (addr <= 0x0FFF) {
            val = chr[((banks[5]) * 0x400 + (addr - 0xC00)) % chr_size];
        } else if (addr <= 0x17FF) {
            val = chr[((banks[0] >> 1u) * 0x800 + (addr - 0x1000)) % chr_size];
        } else if (addr <= 0x1FFF) {
            val = chr[((banks[1] >> 1u) * 0x800 + (addr - 0x1800)) % chr_size];
        }
    } else {
        if (addr <= 0x07FF) {
            val = chr[((banks[0] >> 1u) * 0x800 + (addr)) % chr_size];
        } else if (addr <= 0x0FFF) {
            val = chr[((banks[1] >> 1u) * 0x800 + (addr - 0x800)) % chr_size];
        } else if (addr <= 0x13FF) {
            val = chr[((banks[2]) * 0x400 + (addr - 0x1000)) % chr_size];
        } else if (addr <= 0x17FF) {
            val = chr[((banks[3]) * 0x400 + (addr - 0x1400)) % chr_size];
        } else if (addr <= 0x1BFF) {
            val = chr[((banks[4]) * 0x400 + (addr - 0x1800)) % chr_size];
        } else if (addr <= 0x1FFF) {
            val = chr[((banks[5]) * 0x400 + (addr - 0x1C00)) % chr_size];
        }
    }
    return val;
}

void MMC3::chr_write(uint16_t addr, uint8_t val) { chr[addr] = val; }

void MMC3::on_scanline_done() {
    if (irq_counter == 0) {
        irq_counter = irq_latch;
    } else {
        --irq_counter;
    }
    if (irq_enabled && irq_counter == 0) {
        irq_handler(true);
    }
}
