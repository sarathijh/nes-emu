#ifndef UxROM_HPP
#define UxROM_HPP

#include "../mapper.hpp"

class UxROM : public Mapper {
private:
    uint8_t bank_select = 0;

public:
    using Mapper::Mapper;

public:
    uint8_t read(uint16_t addr) override;
    void write(uint16_t addr, uint8_t val) override;
    uint8_t chr_read(uint16_t addr) override;
    void chr_write(uint16_t addr, uint8_t val) override;
};

#endif // UxROM_HPP
