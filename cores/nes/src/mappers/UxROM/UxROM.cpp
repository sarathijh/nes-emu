#include "UxROM.hpp"

uint8_t UxROM::read(uint16_t addr) {
    if (addr < 0x8000) {
        return prg_ram[addr - 0x6000];
    } else if (addr < 0xC000) {
        return prg[(bank_select * 0x4000 + (addr - 0x8000)) % prg_size];
    } else {
        return prg[((prg_size - 0x4000) + (addr - 0xC000)) % prg_size];
    }
}

void UxROM::write(uint16_t addr, uint8_t val) {
    if (addr < 0x8000) {
        prg_ram[addr - 0x6000] = val;
    } else {
        bank_select = val;
    }
}

uint8_t UxROM::chr_read(uint16_t addr) { return chr[addr]; }

void UxROM::chr_write(uint16_t addr, uint8_t val) { chr[addr] = val; }
