#include "mapper.hpp"

Mapper::Mapper(uint8_t *rom) {
    prg_size = rom[4] * 0x4000;
    chr_size = rom[5] * 0x2000;
    mirroring = (rom[6] & 1u ? Mirroring::VERTICAL : Mirroring::HORIZONTAL);
    battery_backed_ram_exists = (unsigned) (rom[6] >> 1u) & 1u;

    prg_ram_size = rom[8] * 0x2000;
    if (prg_ram_size == 0) {
        prg_ram_size = 0x2000;
    }
    prg_ram = new uint8_t[prg_ram_size];

    prg = rom + 16;

    if ((chr_ram_exists = (chr_size == 0))) {
        chr_size = 0x2000;
        chr = new uint8_t[chr_size];
    } else {
        chr = prg + prg_size;
    }
}

Mapper::~Mapper() {
    delete prg_ram;
    if (chr_ram_exists) {
        delete chr;
    }
}

bool Mapper::has_battery_backed_ram() { return battery_backed_ram_exists; }

uint8_t *Mapper::ram() { return prg_ram; }

int Mapper::ram_size() { return prg_ram_size; }
