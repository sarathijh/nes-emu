#ifndef MAPPER_HPP
#define MAPPER_HPP

#include <functional>
#include <iostream>

enum class Mirroring { VERTICAL = 0, HORIZONTAL = 1 };

class Mapper {
public:
    std::function<void(bool)> irq_handler;

protected:
    uint8_t *prg, *prg_ram, *chr;
    int prg_size, prg_ram_size, chr_size;

    bool chr_ram_exists, battery_backed_ram_exists;

public:
    Mirroring mirroring; // TODO: Make private

public:
    explicit Mapper(uint8_t *rom);
    virtual ~Mapper();

public:
    bool has_battery_backed_ram();
    uint8_t *ram();
    int ram_size();

public:
    virtual uint8_t read(uint16_t addr) = 0;
    virtual void write(uint16_t addr, uint8_t val) = 0;
    virtual uint8_t chr_read(uint16_t addr) = 0;
    virtual void chr_write(uint16_t addr, uint8_t val) = 0;
    virtual void on_scanline_done() {}
};

#endif // MAPPER_HPP
