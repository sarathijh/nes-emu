#ifndef NESEMU_PPU_HPP
#define NESEMU_PPU_HPP

#include "cart.hpp"
#include <mos6502/imemory.hpp>

constexpr unsigned NES_RGB[] = {
   0x7C7C7C, 0x0000FC, 0x0000BC, 0x4428BC, 0x940084, 0xA80020, 0xA81000, 0x881400,
   0x503000, 0x007800, 0x006800, 0x005800, 0x004058, 0x000000, 0x000000, 0x000000,
   0xBCBCBC, 0x0078F8, 0x0058F8, 0x6844FC, 0xD800CC, 0xE40058, 0xF83800, 0xE45C10,
   0xAC7C00, 0x00B800, 0x00A800, 0x00A844, 0x008888, 0x000000, 0x000000, 0x000000,
   0xF8F8F8, 0x3CBCFC, 0x6888FC, 0x9878F8, 0xF878F8, 0xF85898, 0xF87858, 0xFCA044,
   0xF8B800, 0xB8F818, 0x58D854, 0x58F898, 0x00E8D8, 0x787878, 0x000000, 0x000000,
   0xFCFCFC, 0xA4E4FC, 0xB8B8F8, 0xD8B8F8, 0xF8B8F8, 0xF8A4C0, 0xF0D0B0, 0xFCE0A8,
   0xF8D878, 0xD8F878, 0xB8F8B8, 0xB8F8D8, 0x00FCFC, 0xF8D8F8, 0x000000, 0x000000,
  };

/**
 * Emulates the Picture Processing Unit used in the NES.
 */
class PPU : public IMemory {
public:
  std::function<void()> nmi_handler;
  std::function<void(unsigned*)> post_handler;
  std::function<void()> on_scanline_done;
  
  
private:
  union Ctrl {
    uint8_t bits;
    struct {
      // Base nametable address
      // (0 = $2000; 1 = $2400; 2 = $2800; 3 = $2C00)
      unsigned nt: 2;
      // VRAM address increment per CPU read/write of PPUDATA
      // (0: add 1, going across; 1: add 32, going down)
      unsigned increment: 1;
      // Sprite pattern table address for 8x8 sprites
      // (0: $0000; 1: $1000; ignored in 8x16 mode)
      unsigned sprTable: 1;
      // Background pattern table address (0: $0000; 1: $1000)
      unsigned bgTable: 1;
      // Sprite size (0: 8x8; 1: 8x16)
      unsigned sprSize: 1;
      // PPU master/slave select
      // (0: read backdrop from EXT pins; 1: output color on EXT pins)
      unsigned slave: 1;
      // Generate an NMI at the start of the vertical blanking interval
      // (0: off; 1: on)
      unsigned nmi: 1;
    };
  };

  union Mask {
    uint8_t bits;
    struct {
      // Grayscale (0: normal color, 1: produce a grayscale display)
      unsigned gray: 1;
      // 1: Show background in leftmost 8 pixels of screen, 0: Hide
      unsigned bgLeft: 1;
      // 1: Show sprites in leftmost 8 pixels of screen, 0: Hide
      unsigned sprLeft: 1;
      // 1: Show background
      unsigned bgShow: 1;
      // 1: Show sprites
      unsigned sprShow: 1;
      // Emphasize red
      unsigned red: 1;
      // Emphasize green
      unsigned green: 1;
      // Emphasize green
      unsigned blue: 1;
    };
  };

  union Status {
    uint8_t bits;
    struct {
      // Least significant bits previously written into a PPU register
      // (due to register not being updated for this address)
      unsigned _: 5;
      // Sprite overflow. The intent was for this flag to be set
      // whenever more than eight sprites appear on a scanline, but a
      // hardware bug causes the actual behavior to be more complicated
      // and generate false positives as well as false negatives; see
      // PPU sprite evaluation. This flag is set during sprite
      // evaluation and cleared at dot 1 (the second dot) of the
      // pre-render line.
      unsigned sprOverflow: 1;
      // Sprite 0 Hit.  Set when a nonzero pixel of sprite 0 overlaps
      // a nonzero background pixel; cleared at dot 1 of the pre-render
      // line.  Used for raster timing.
      unsigned sprHit: 1;
      // Vertical blank has started (0: not in vblank; 1: in vblank).
      // Set at dot 1 of line 241 (the line *after* the post-render
      // line); cleared after reading $2002 and at dot 1 of the
      // pre-render line.
      unsigned vBlank: 1;
    };
  };

  union Addr {
    unsigned bits: 15;
    unsigned addr: 14;
    struct {
      unsigned coarseX: 5;
      unsigned coarseY: 5;
      unsigned nt: 2;
      unsigned fineY: 3;
    };
    struct {
      unsigned lo: 8;
      unsigned hi: 7;
    };
  };

  struct Sprite {
    uint8_t id, x, y, tile, attr, bitmapL, bitmapH;
    
    void clear () {
      id = 64;
      x = y = tile = attr = 0xFF;
      bitmapL = bitmapH = 0x00;
    }
    
    void load_oam (int id, uint8_t *oam) {
      this->id   = id;
      this->y    = oam [0];
      this->tile = oam [1];
      this->attr = oam [2];
      this->x    = oam [3];
    }
    
    uint8_t get_pixel (int screenX) {
      // Empty sprite loaded with all transparent bitmap.
      if (id == 64) { return 0; }
      
      unsigned sprX = screenX - x;
      if (sprX >= 8) { return 0; } // Already past right side of sprite.
      if (attr & 0x40) { sprX ^= 7; } // Horizontal flip.
      
      return calc_palette (bitmapH, bitmapL, 7 - sprX);
    }
  };
  
  
private:
  Cart *cart;
  
  Addr  v;     // Current VRAM address (15 bits)
  Addr  t;     // Temporary VRAM address (15 bits); can also be thought of as the address of the top left onscreen tile.
  uint8_t fineX; // Fine X scroll (3 bits)
  bool  w;     // First or second write toggle (1 bit)
  
  uint8_t ciRam  [0x800]; // 2KB of VRAM for nametables
  uint8_t cgRam  [0x20];  //  32 bytes of VRAM for palettes
  uint8_t oamRam [0x100]; // 256 bytes of VRAM for sprite properties
  
  // sprite buffers
  Sprite oam [8], secOam [8];
  
  uint8_t oamAddr;
  
  Ctrl   ctrl;   // PPUCTRL   0x2000 register
  Mask   mask;   // PPUMASK   0x2001 register
  Status status; // PPUSTATUS 0x2002 register
  
  // background latches
  uint8_t nt, at, bgH, bgL;
  
  // background shift registers
  uint16_t bgShiftH, bgShiftL;
  uint8_t  atShiftH, atShiftL;
  bool   atLatchH, atLatchL;
  
  int scanline, dot;
  bool isOddFrame;
  
  unsigned pixels [256*240];
  
  
public:
  PPU(Cart *cart) {
    this->cart = cart;
      reset();
  }
  
public:
  void reset () {
    isOddFrame = false;
    scanline = dot = 0;
    ctrl.bits = mask.bits = status.bits = 0;
    
    memset (pixels, 0x00, sizeof (pixels));
    memset (ciRam,  0xFF, sizeof (ciRam));
    memset (oamRam, 0x00, sizeof (oamRam));
  }
  
private:
  bool is_rendering () { return (mask.bgShow or mask.sprShow); }
  
  
private:
  // The NES has four nametables, arranged in a 2x2 pattern.
  // Each occupies a 1 KiB chunk of PPU address space,
  // starting at $2000 at the top left, $2400 at the top right,
  // $2800 at the bottom left, and $2C00 at the bottom right.
  uint16_t nt_addr () { return 0x2000 | (v.bits & 0x0FFF); }
  
  // The attribute table is a 64-byte array at the end of each nametable that
  // controls which palette is assigned to each part of the background.
  // Each attribute table, starting at $23C0, $27C0, $2BC0, or $2FC0,
  // is arranged as an 8x8 byte array:
  // 
  // NN 1111 YYY XXX
  // || |||| ||| +++-- high 3 bits of coarse X (x/4)
  // || |||| +++------ high 3 bits of coarse Y (y/4)
  // || ++++---------- attribute offset (960 bytes)
  // ++--------------- nametable select
  uint16_t at_addr () { return 0x23C0 | (v.bits & 0x0C00) | ((v.coarseY / 4) << 3) | (v.coarseX / 4); }
  
  uint16_t bg_addr () { return (ctrl.bgTable * 0x1000) + (nt << 4) + v.fineY; }
  
  
private:
  void reload_shifters () {
    bgShiftL = (bgShiftL & 0xFF00) | bgL;
    bgShiftH = (bgShiftH & 0xFF00) | bgH;

    atLatchL = (at & 1);
    atLatchH = (at & 2);
  }
  
  uint16_t mirror_nt (uint16_t addr) {
    switch (cart->mirroring ()) {
      case Mirroring::VERTICAL:   return addr % 0x800;
      case Mirroring::HORIZONTAL: return ((addr / 2) & 0x400) + (addr % 0x400);
      default:                    return addr - 0x2000;
    }
  }
  
  
private:
  uint8_t load (uint16_t addr) {
    if (addr <= 0x1FFF) { return cart->chr_read (addr);    } else // CHR-ROM/RAM
    if (addr <= 0x3EFF) { return ciRam [mirror_nt (addr)]; } else // Name tables
    if (addr <= 0x3F1F) { // Palettes
      // $3F10/$3F14/$3F18/$3F1C are mirrors of $3F00/$3F04/$3F08/$3F0C.
      // Note that this goes for writing as well as reading.
      // A symptom of not having implemented this correctly in an emulator is
      // the sky being black in Super Mario Bros., which writes the backdrop
      // color through $3F10.
      if ((addr & 0x13) == 0x10) { addr &= ~0x10; }
      return cgRam [addr & 0x1F] & (mask.gray ? 0x30 : 0xFF);
    }
    return 0;
  }
  
  void store (uint16_t addr, uint8_t val) {
    if (addr <= 0x1FFF) { cart->chr_write (addr, val);    } else // CHR-ROM/RAM
    if (addr <= 0x3EFF) { ciRam [mirror_nt (addr)] = val; } else // Name tables
    if (addr <= 0x3F1F) { // Palettes
      if ((addr & 0x13) == 0x10) { addr &= ~0x10; }
      cgRam [addr & 0x1F] = val;
    }
  }
  
  
public:
  uint8_t read (uint16_t index) override {
    static uint8_t res = 0;
    static uint8_t buf = 0;
    
    if (index == 2) { res = (res & 0x1F) | status.bits; status.vBlank = w = 0; } else // PPUSTATUS
    if (index == 4) { res = oamRam [oamAddr];                                  } else // OAMDATA
    if (index == 7) { // PPUDATA
      if (v.addr <= 0x3EFF) {
        res = buf; buf = load (v.addr);
      } else {
        res = buf = load (v.addr);
      }
      v.addr += ctrl.increment ? 32 : 1;
    }
    
    return res;
  }
  
  void write (uint16_t index, uint8_t val) override {
    if (index == 0) { ctrl.bits = val; t.nt = ctrl.nt; } else // PPUCTRL
    if (index == 1) { mask.bits = val;                 } else // PPUMASK
    if (index == 3) { oamAddr   = val;                 } else // OAMADDR
    if (index == 4) { oamRam [oamAddr++] = val;        } else // OAMDATA
    if (index == 5) { // PPUSCROLL
      if ((w = !w)) {   fineX = val & 7; t.coarseX = val >> 3; }
               else { t.fineY = val & 7; t.coarseY = val >> 3; }
    } else
    if (index == 6) { // PPUADDR
      // First write (w == 0)
      // t: .FEDCBA ........ = d: ..FEDCBA
      // t: X...... ........ = 0
      // w:                  = 1
      if ((w = !w)) { t.hi = val & 0x3F; }
      // Second write (w == 1)
      // t: ....... HGFEDCBA = d: HGFEDCBA
      // v                   = t
      // w:                  = 0
      else { t.lo = val; v.bits = t.bits; }
    } else
    if (index == 7) { // PPUDATA
      store (v.addr, val);
      v.addr += ctrl.increment ? 32 : 1;
    }
  }
  
  
private:
  void sprite_evaluation () {
    int n = 0;
    // OAM can hold 64 sprites.
    for (int i = 0; i < 64; ++i) {
      // Check if sprite is on the current scanline.
      int line = (scanline == 261 ? -1 : scanline) - oamRam [i*4 + 0];
      if (0 <= line and line < (ctrl.sprSize ? 16 : 8)) {
        // Can only have 8 sprites per scanline.
        // The "sprite overflow bug" is not implemented.
        if (n == 8) { status.sprOverflow = 1; break; }
        // If it is, then load it into secondary OAM.
        secOam [n++].load_oam (i, oamRam + (i*4));
      }
    }
  }
  
  void load_sprite_data () {
    uint16_t addr = 0;
    for (int i = 0; i < 8; ++i) {
      Sprite &spr = oam [i] = secOam [i];
      
      if (ctrl.sprSize) {
        addr = ((spr.tile & 1) * 0x1000) + ((spr.tile & ~1) * 16);
      } else {
        addr = ( ctrl.sprTable * 0x1000) + ( spr.tile       * 16);
      }
      
      unsigned sprY = (scanline - spr.y) % (ctrl.sprSize ? 16 : 8);
      if (spr.attr & 0x80) { sprY ^= (ctrl.sprSize ? 16 : 8) - 1; }
      addr += sprY + (sprY & 8);

      spr.bitmapL = load (addr + 0);
      spr.bitmapH = load (addr + 8);
    }
  }
  
  
private:
  static uint8_t calc_palette (uint16_t hi, uint16_t lo, uint8_t x) {
    return (((hi >> x) & 1) << 1) | ((lo >> x) & 1);
  }
  
  void draw_pixel () {
    uint8_t palette = 0;
    uint8_t objPalette = 0;
    bool objPriority = 0;
    int x = dot - 2;
    
    if (scanline < 240 and 0 <= x and x < 256) {
      // Background
      if (mask.bgShow and (x >= 8 or mask.bgLeft)) {
        palette = calc_palette (bgShiftH, bgShiftL, 15 - fineX);
        if (palette) {
          palette |= calc_palette (atShiftH, atShiftL, 7 - fineX) << 2;
        }
      }
      // Sprites
      if (mask.sprShow and (mask.sprLeft or x >= 8)) {
        for (int i = 7; i >= 0; --i) {
          Sprite &spr = oam [i];
          
          uint8_t sprPalette = spr.get_pixel (x);
          if (sprPalette) {
            if (spr.id == 0 and palette and x != 255) { status.sprHit = 1; }
            
            objPalette  = (((spr.attr & 0b11) << 2) | sprPalette) + 16;
            objPriority = spr.attr & 0x20;
          }
        }
      }
      if (objPalette and (palette == 0 or objPriority == 0)) { palette = objPalette; }
      pixels [scanline*256 + x] = NES_RGB [load (0x3F00 + (is_rendering () ? palette : 0))];
    }
    
    bgShiftL <<= 1;
    bgShiftH <<= 1;
    atShiftL = (atShiftL << 1) | atLatchL;
    atShiftH = (atShiftH << 1) | atLatchH;
  }
  
  
public:
  void tick () {
    // Post-renderer line
    if (scanline == 240 and dot == 0) { post_handler (pixels); } else
    if (scanline == 241 and dot == 1) { status.vBlank = 1; if (ctrl.nmi) { nmi_handler (); } }
    else {
      bool isPre = (scanline == 261);
      if (scanline <= 239 or isPre) {
        // Sprites
        if (dot == 1) {
          if (isPre) { status.sprHit = status.sprOverflow = 0; }
          for (int i = 0; i < 8; ++i) { secOam [i].clear (); }
        }
        else if (dot == 257) { sprite_evaluation (); }
        else if (dot == 321) { load_sprite_data (); }
        
        // Background Visible Frame
        static uint16_t addr;
        if (dot == 1) { addr = nt_addr (); if (isPre) { status.vBlank = 0; } }
        else if ((2 <= dot and dot <= 256) or (322 <= dot and dot <= 337)) {
          draw_pixel ();
          switch (dot % 8) {
            // Nametable byte
            // The shifters are reloaded during ticks 9, 17, 25, ..., 257, 329, 337.
            case 1: addr = nt_addr (); reload_shifters (); break;
            case 2:   nt = load (addr); break;
            
            // Attribute table byte
            case 3: addr = at_addr (); break;
            case 4:   at = load (addr);
              if (v.coarseY & 2) { at >>= 4; }
              if (v.coarseX & 2) { at >>= 2; }
              break;
            
            // Tile bitmap low
            case 5: addr = bg_addr (); break;
            case 6:  bgL = load (addr); break;
            
            // Tile bitmap high (+8 bytes from tile bitmap low)
            case 7: addr += 8; break;
            case 0: bgH = load (addr);
              if (is_rendering ()) {
                if (dot == 256) {
                  // Increment vertical position.
                  // If we hit the end of the current tile, then perform coarse increment.
                  // If we hit the last tile in the current nametable,
                  // then switch the vertical nametable.
                  if (++v.fineY == 0 and ++v.coarseY == 30) { v.coarseY = 0; v.nt ^= 0b10; }
                } else {
                  // Increment horizontal position.
                  // If we hit the last tile in the current nametable,
                  // then switch the horizontal nametable.
                  if (++v.coarseX == 0) { v.nt ^= 0b01; }
                }
              }
          }
        }
        // Update horizontal position.
        else if (dot == 257) {
          draw_pixel ();
          reload_shifters ();
          if (is_rendering ()) {
            // Replace v's coarseX and horizontal nametable with t's.
            v.bits = (v.bits & ~0x041F) | (t.bits & 0x041F);
          }
        }
        // Update vertical position.
        else if (280 <= dot and dot <= 304) {
          if (isPre && is_rendering ()) {
            // Replace v's fineY, coarseY, and vertical nametable with t's.
            v.bits = (v.bits & ~0x7BE0) | (t.bits & 0x7BE0);
          }
        }
        else if (dot == 321 or dot == 339) { addr = nt_addr (); }
        // Unused NT fetches
        else if (dot == 338 or dot == 340) { nt = load (addr); }
        
        if (dot == 340) { if (is_rendering()) { on_scanline_done(); }}
      }
    }
    
    if (++dot > 340) {
      dot = 0;
      if (++scanline > 261) {
        scanline = 0;
        if ((isOddFrame = !isOddFrame)) { dot = 1; }
      }
    }
  }
};

#endif // NESEMU_PPU_HPP
