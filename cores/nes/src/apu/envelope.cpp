#include "envelope.hpp"

Envelope::Envelope() : divider(15, []() {}) {}

void Envelope::tick() {
    if (!start) {
        divider.tick();
    } else {
        start = false;
        decay = 15;
        divider.reset();
    }
}
