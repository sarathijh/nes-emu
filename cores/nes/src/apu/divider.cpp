#include "divider.hpp"

Divider::Divider(int period, const std::function<void()> &on_tick) {
    this->period = period;
    this->count = period;
    this->on_tick = on_tick;
}

void Divider::set_period(int value) { period = value; }

void Divider::tick() {
    --count;
    if (count == 0) {
        on_tick();
        count = period;
    }
}

void Divider::reset() { count = period; }
