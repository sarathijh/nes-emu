#ifndef NESEMU_DIVIDER_HPP
#define NESEMU_DIVIDER_HPP

#include <functional>

class Divider {
private:
    int period;
    int count;
    std::function<void()> on_tick;

public:
    explicit Divider(int period, const std::function<void()> &on_tick);

public:
    void set_period(int value);
    void tick();
    void reset();
};

#endif // NESEMU_DIVIDER_HPP
