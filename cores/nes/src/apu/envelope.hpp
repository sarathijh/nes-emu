#ifndef NESEMU_ENVELOPE_HPP
#define NESEMU_ENVELOPE_HPP

#include "divider.hpp"

class Envelope {
private:
    bool start = false;
    Divider divider;
    uint8_t decay = 0;

public:
    Envelope();

public:
    void tick();
};

#endif // NESEMU_ENVELOPE_HPP
