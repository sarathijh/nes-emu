#include "pulsechannel.hpp"

PulseChannel::PulseChannel(bool one) : one(one) {}

void PulseChannel::write(int index, uint8_t val) {
    switch (index) {
    case 0:
        volume = val & 15u;
        const_volume = (unsigned) (val >> 4u) & 1u;
        halt = (unsigned) (val >> 5u) & 1u;
        duty_cycle = (unsigned) (val >> 6u) & 0b11u;
        break;
    case 1: sweep.bits = val; break;
    case 2:
        t |= val;
        timer = t;
        break;
    case 3:
        load_length_counter(val);
        t |= (((uint16_t) val & 0x7u) << 8u);
        timer = t;
    default: break;
    }
}

void PulseChannel::update_target_period() {
    target_period = period + ((unsigned) (timer >> sweep.shift) ^
                              (0xFFFFu * sweep.negate) - one);
    if (target_period > 0x7FF) {
        muted = true;
    }
}

void PulseChannel::tick() {
    if (timer == 0) {
        generate(volume);
        timer = t;
    } else {
        --timer;
    }
    if (period < 8) {
        muted = true;
    }
}
