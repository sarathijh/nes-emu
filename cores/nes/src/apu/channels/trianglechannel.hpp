#ifndef NESEMU_TRIANGLECHANNEL_HPP
#define NESEMU_TRIANGLECHANNEL_HPP

#include "../envelope.hpp"
#include "channel.hpp"

class TriangleChannel : public Channel {
private:
    Envelope envelope;

public:
    void write(int index, uint8_t val);
};

#endif // NESEMU_TRIANGLECHANNEL_HPP
