#ifndef NESEMU_PULSECHANNEL_HPP
#define NESEMU_PULSECHANNEL_HPP

#include "../envelope.hpp"
#include "channel.hpp"

#include <functional>

class PulseChannel : public Channel {
private:
    union {
        uint8_t bits;
        struct {
            unsigned shift : 3;
            unsigned negate : 1;
            unsigned period : 3;
            unsigned enabled : 1;
        };
    } sweep {};

public:
    std::function<void(uint8_t)> generate;

private:
    uint8_t duty_cycle = 0;
    uint8_t volume = 0;
    bool const_volume = false;
    uint16_t t = 0;
    uint16_t timer = 0;
    uint16_t period = 0;
    uint16_t target_period = 0;
    bool one = false;
    bool muted = false;

    Envelope envelope {};

public:
    explicit PulseChannel(bool one);

public:
    void write(int index, uint8_t val);
    void update_target_period();
    void tick();
};

#endif // NESEMU_PULSECHANNEL_HPP
