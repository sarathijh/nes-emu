#ifndef NESEMU_DELTAMODULATIONCHANNEL_HPP
#define NESEMU_DELTAMODULATIONCHANNEL_HPP

#include "channel.hpp"

#include <cstdint>

class DeltaModulationChannel : public Channel {
private:
    uint8_t output_level = 0;
    uint8_t counter = 0;
    uint16_t sample_length = 0;
    uint16_t sample_addr = 0;
    unsigned frequency = 0;
    bool irq = false;
    bool loop = false;

public:
    void increment_counter(bool up);
    void write(int index, uint8_t val);
};

#endif // NESEMU_DELTAMODULATIONCHANNEL_HPP
