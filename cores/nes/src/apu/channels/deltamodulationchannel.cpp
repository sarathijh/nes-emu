#include "deltamodulationchannel.hpp"

unsigned frequency_lookup(uint8_t index);

void DeltaModulationChannel::increment_counter(bool up) {
    // A bit of 1 will increment the counter, 0 will decrement.
    counter += 2 * up - 1;
    // It will clamp rather than overflow if the 7-bit range is exceeded.
    counter &= 0x7Fu;
}

void DeltaModulationChannel::write(int index, uint8_t val) {
    switch (index) {
    case 0:
        irq = val & 0x80u;
        loop = val & 0x40u;
        frequency = val & 0b1111u;
        break;
    case 1: output_level = val & 0x7Fu; break;
    case 2: sample_addr = 0xC000 + ((uint16_t) val * 64); break;
    case 3: sample_length = ((uint16_t) val * 16) + 1; break;
    default: break;
    }
}

unsigned frequency_lookup(uint8_t index) {
    switch (index) {
    case 0x0: return 428;
    case 0x6: return 226;
    case 0xC: return 106;
    case 0x1: return 380;
    case 0x7: return 214;
    case 0xD: return 84;
    case 0x2: return 340;
    case 0x8: return 190;
    case 0xE: return 72;
    case 0x3: return 320;
    case 0x9: return 160;
    case 0xF: return 54;
    case 0x4: return 286;
    case 0xA: return 142;
    case 0x5: return 254;
    case 0xB: return 128;
    default: return 0;
    }
}
