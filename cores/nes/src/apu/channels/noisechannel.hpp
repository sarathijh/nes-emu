#ifndef NESEMU_NOISECHANNEL_HPP
#define NESEMU_NOISECHANNEL_HPP

#include "../envelope.hpp"
#include "channel.hpp"

class NoiseChannel : public Channel {
private:
    Envelope envelope;

public:
    void write(int index, uint8_t val);
};

#endif // NESEMU_NOISECHANNEL_HPP
