#include "channel.hpp"

uint8_t lc_table(uint8_t val);

void Channel::set_enabled(bool enabled) {
    this->enabled = enabled;
    if (enabled == 0) {
        length_counter = 0;
    }
}

void Channel::load_length_counter(uint8_t val) {
    length_counter = lc_table(val & 0xF8u);
}

void Channel::quarter_frame() {}

void Channel::half_frame() {
    if (length_counter > 0 and halt == 0) {
        --length_counter;
    }
}

uint8_t lc_table(uint8_t val) {
    switch (val) {
        // Legend:
        // <bit pattern> (<value of bit pattern>) => <note length>

        // Linear length values:
    case 0x1F: return 30;
    case 0x1D: return 28;
    case 0x1B: return 26;
    case 0x19: return 24;
    case 0x17: return 22;
    case 0x15: return 20;
    case 0x13: return 18;
    case 0x11: return 16;
    case 0x0F: return 14;
    case 0x0D: return 12;
    case 0x0B: return 10;
    case 0x09: return 8;
    case 0x07: return 6;
    case 0x05: return 4;
    case 0x03: return 2;
    case 0x01:
        return 254;

        // Notes with base length 12 (4/4 at 75 bpm):
    case 0x1E: return 32; // (96 times 1/3, quarter note triplet)
    case 0x1C: return 16; // (48 times 1/3, eighth note triplet)
    case 0x1A: return 72; // (48 times 1 1/2, dotted quarter)
    case 0x18: return 192; // (Whole note)
    case 0x16: return 96; // (Half note)
    case 0x14: return 48; // (Quarter note)
    case 0x12: return 24; // (Eighth note)
    case 0x10:
        return 12; // (Sixteenth)

        // Notes with base length 10 (4/4 at 90 bpm, with relative durations
        // being the same as above):
    case 0x0E: return 26; // (Approx. 80 times 1/3, quarter note triplet)
    case 0x0C: return 14; // (Approx. 40 times 1/3, eighth note triplet)
    case 0x0A: return 60; // (40 times 1 1/2, dotted quarter)
    case 0x08: return 160; // (Whole note)
    case 0x06: return 80; // (Half note)
    case 0x04: return 40; // (Quarter note)
    case 0x02: return 20; // (Eighth note)
    case 0x00: return 10; // (Sixteenth)

    default: return 0;
    }
}
