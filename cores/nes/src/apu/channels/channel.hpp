#ifndef NESEMU_CHANNEL_HPP
#define NESEMU_CHANNEL_HPP

#include <cstdint>

class Channel {
protected:
    bool enabled = false;
    bool halt = false;
    uint8_t length_counter = 0;

public:
    void set_enabled(bool enabled);
    void load_length_counter(uint8_t val);
    void quarter_frame();
    void half_frame();

public:
    virtual float value() { return 0; }
};

#endif // NESEMU_CHANNEL_HPP
