#ifndef APU_H
#define APU_H

#include <iostream>
#include <iomanip>
#include <map>
#include "channels/pulsechannel.hpp"
#include "channels/deltamodulationchannel.hpp"
#include "channels/trianglechannel.hpp"
#include "channels/noisechannel.hpp"
#include "divider.hpp"
#include "envelope.hpp"
#include "channels/channel.hpp"
#include <mos6502/imemory.hpp>

class APU : public IMemory {
private:
    union Status {
        uint8_t bits;
        struct {
            unsigned pulse1: 1;
            unsigned pulse2: 1;
            unsigned triangle: 1;
            unsigned noise: 1;
            unsigned dmc: 1;
            unsigned _: 1;
            unsigned int_frame: 1;
            unsigned int_dmc: 1;
        };
    };

public:
    std::function<void()> irq_handler;
    std::function<void(uint8_t)> sample_handler;

private:
    PulseChannel pulse1;
    PulseChannel pulse2;
    TriangleChannel triangle;
    NoiseChannel noise;
    DeltaModulationChannel deltaModulationChannel;

    bool mode = false;
    bool interrupt = false;
    int half_cycles = 0;
    int reset_timer = 0;

public:
    APU() : pulse1(true), pulse2(false) {
        pulse1.generate = [&](uint8_t sample) { sample_handler(sample); };
    }

public:
    uint8_t read(uint16_t addr) override {
        if (addr == 0x4015) {
            return read_channels_enabled();
        }
        return 0;
    }

    void write(uint16_t addr, uint8_t val) override {
        if (addr <= 0x4003) { pulse1.write(addr % 4, val); }
        else if (addr <= 0x4007) { pulse2.write(addr % 4, val); }
        else if (addr <= 0x400B) { triangle.write(addr % 4, val); }
        else if (addr <= 0x400F) { noise.write(addr % 4, val); }
        else if (addr <= 0x4013) { deltaModulationChannel.write(addr % 4, val); }
        else if (addr == 0x4015) { write_channels_enabled(val); }
        else if (addr == 0x4017) { write_frame_counter(val); }
    }

private:
    float mixer_output() {
        return pulse_output() + tnd_output();
    }

    float pulse_output() {
        return 95.52f / (8128.0f / (pulse1.value() + pulse2.value()) + 100);
    }

    float tnd_output() {
        return 163.67f / (24329.0f / (3 * triangle.value() + 2 * noise.value() + deltaModulationChannel.value()) + 100);
    }

private:
    void write_channels_enabled(uint8_t val) {
        pulse1.set_enabled((unsigned) (val >> 0u) & 1u);
        pulse2.set_enabled((unsigned) (val >> 1u) & 1u);
        triangle.set_enabled((unsigned) (val >> 2u) & 1u);
        noise.set_enabled((unsigned) (val >> 3u) & 1u);
        deltaModulationChannel.set_enabled((unsigned) (val >> 4u) & 1u);
    }

    uint8_t read_channels_enabled() {
        return 0;
    }

    void write_frame_counter(uint8_t val) {
        interrupt = (unsigned) (val >> 6u) & 1u;
        mode = (unsigned) (val >> 7u) & 1u;
        reset_timer = half_cycles % 2 == 0 ? 3 : 4;
    }

public:
    void tick() {
        if (--reset_timer == 0) {
            half_cycles = 0;
            if (mode == 1) { half_frame(); }
        } else {
            ++half_cycles;
        }

        if (half_cycles % 2 == 1) {
            pulse1.tick();
        }

        if (half_cycles % 20000 == 0) {
            sample_handler(5);
        }

        if (mode == 0) {
            switch (half_cycles) {
                case 7457: quarter_frame();
                    break;
                case 14913: half_frame();
                    break;
                case 22371: quarter_frame();
                    break;
                case 29828: if (interrupt == 0) { irq_handler(); }
                    break;
                case 29829: half_frame();
                    if (interrupt == 0) { irq_handler(); }
                    break;
                case 29830: half_cycles = 0;
                    if (interrupt == 0) { irq_handler(); }
                    break;
            }
        } else {
            switch (half_cycles) {
                case 7457: quarter_frame();
                    break;
                case 14913: half_frame();
                    break;
                case 22371: quarter_frame();
                    break;
                case 29829: break;
                case 37281: half_frame();
                    break;
                case 37282: half_cycles = 0;
                    break;
            }
        }
    }

private:
    void quarter_frame() {
        pulse1.quarter_frame();
    }

    void half_frame() {
        quarter_frame();
        pulse1.half_frame();
    }
};

#endif // APU_H
