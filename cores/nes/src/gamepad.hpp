#ifndef NESEMU_GAMEPAD_HPP
#define NESEMU_GAMEPAD_HPP

#include <memory>

/**
 * The Gamepad class manages the state of the buttons for the two gamepads.
 */
class Gamepad {
private:
    /// The button states for the two gamepads.
    /// Status for each controller is returned as an 8-bit report in the
    /// following order: A, B, Select, Start, Up, Down, Left, Right.
    uint8_t buttons[2] {0, 0};

    /// The shift registers that return the state of the next button.
    uint8_t shift_regs[2] {0, 0};

    /// The strobe controls loading the shift registers with the button states.
    bool strobe = false;

public:
    /**
     * Each read reports one bit at a time. The first 8 reads will
     * indicate which buttons or directions are pressed (1 if pressed, 0 if not
     * pressed). All subsequent reads will return 1 on official Nintendo brand
     * controllers but may return 0 on third party controllers such as the
     * U-Force. Status for each controller is returned as an 8-bit report in the
     * following order: A, B, Select, Start, Up, Down, Left, Right.
     *
     * @param port - which controller to read (0 or 1)
     * @return 1 if pressed, 0 if not pressed
     */
    uint8_t read_button(unsigned port);

    /**
     * While the strobe is high, the shift registers in the controllers are
     * continuously reloaded from the button states, and reading will keep
     * returning the current state of the first button (A). Once the strobe goes
     * low, this reloading will stop. Hence a 1/0 write sequence is required to
     * get the button states, after which the buttons can be read back one at a
     * time.
     *
     * @param state - true if high, false if low
     */
    void write_strobe(bool state);

public:
    /**
     * Write the state of a single button on the gamepad in the given port.
     *
     * @param port - which controller to write (0 or 1)
     * @param button - which button to write (0-7)
     * @param state - 1 if pressed, 0 if not pressed
     */
    void set_button_state(unsigned port, unsigned button, unsigned state);
};

#endif // NESEMU_GAMEPAD_HPP
