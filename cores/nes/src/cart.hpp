#ifndef NESEMU_CART_HPP
#define NESEMU_CART_HPP

#include "mappers/MMC1/MMC1.hpp"
#include "mappers/MMC3/MMC3.hpp"
#include "mappers/NROM/NROM.hpp"
#include "mappers/UxROM/UxROM.hpp"
#include "mappers/mapper.hpp"

#include <fstream>
#include <functional>
#include <mos6502/imemory.hpp>

class Cart : public IMemory {
public:
    std::function<void(bool)> irq_handler;

private:
    Mapper *mapper = nullptr;
    uint8_t *rom = nullptr;

public:
    ~Cart();

public:
    uint8_t read(uint16_t addr) override;
    void write(uint16_t addr, uint8_t val) override;
    uint8_t chr_read(uint16_t addr);
    void chr_write(uint16_t addr, uint8_t val);

public:
    Mirroring mirroring();
    void on_scanline_done();
    bool has_battery_backed_ram();
    uint8_t *ram();
    int ram_size() { return mapper->ram_size(); }

public:
    void load_rom(const std::string &filename);
};

#endif // NESEMU_CART_HPP
