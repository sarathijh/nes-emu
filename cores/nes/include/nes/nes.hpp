#ifndef NES_HPP
#define NES_HPP

#include <functional>
#include <string>

class PPU;
class APU;
class Cart;
class Gamepad;
class MappedMemory;
class Mos6502;

class NES {
private:
    PPU *ppu;
    APU *apu;
    Cart *cart;
    Gamepad *gamepad;
    MappedMemory *mapped_memory;
    Mos6502 *cpu;

    std::string save_file_path;

public:
    std::function<void(unsigned *)> frame_handler;
    std::function<void(uint8_t)> audio_handler;

public:
    explicit NES(const std::string &rom_file_path);
    ~NES();

public:
    void power_on();
    void execute_frame();
    void set_button(int button_index, bool pressed);

private:
    void save();
    void load();
};

#endif // NES_HPP
