#ifndef CPU_H
#define CPU_H

#include <cstdint>
#include <memory>

class CPU {
private:
    union Flags {
        uint8_t bits;
        struct {
            bool _ : 4;
            bool carry : 1;
            bool half_carry : 1;
            bool subtract : 1;
            bool zero : 1;
        };
    };

    uint16_t SP, PC; // Stack Pointer, Program Counter
    uint8_t A; // Accumulator
    uint8_t B, C, D, E, H, L; // General Registers
    Flags F; // Status Register

    uint8_t ram[0x2000]; // 8KB of RAM

    int cycles_remaining;

public:
    void power_on() {
        cycles_remaining = 0;

        PC = 0x100;
        SP = 0xE000;

        A = B = C = D = E = H = L = 0;
        F.bits = 0;

        memset(ram, 0, 0x800);
    }

    void exec_frame() {
        cycles_remaining += 69905;
        while (cycles_remaining > 0) {
            exec_instruction(next());
        }
    }

private:
    /***
     * Clock
     ***/

    void tick() { cycles_remaining -= 1; }

    /***
     * Memory Access
     ***/

    uint8_t read(uint16_t addr) { return 0; }

    uint8_t write(uint16_t addr, uint8_t val) { return 0; }

    uint16_t read16_sep(uint16_t a, uint16_t b) {
        return read(a) | (read(b) << 8);
    }
    uint16_t read16(uint16_t addr) { return read16_sep(addr, addr + 1); }

    uint8_t next() { return read(PC++); }
    uint16_t next16() {
        uint16_t op = read16(PC);
        PC += 2;
        return op;
    }

    uint16_t write16(uint16_t addr, uint16_t val) {
        write(addr, val & 0xFF);
        write(addr + 1, (val >> 8) & 0xFF);
    }

    /***
     * Stack Operations
     ***/

    uint8_t pop() { return read(0x0100 + (++SP)); }
    uint16_t pop16() { return pop() | (pop() << 8); }

    void push(uint8_t val) { write(0x0100 + (SP--), val); }
    void push16(uint16_t val) {
        push(val >> 8);
        push(val & 0xFF);
    }

    /***
     * Update Flags
     ***/

    // void update_carry()

    /***
     * Addressing Modes
     ***/

    uint16_t immediate() { return PC++; }
    uint16_t indirect() { return read(next16()); }

    uint16_t reg16(uint8_t h, uint8_t l) { return ((uint16_t) h << 8) | l; }
    uint16_t BC() { return reg16(B, C); }
    uint16_t DE() { return reg16(D, E); }
    uint16_t HL() { return reg16(H, L); }

    uint16_t indirect_h(uint8_t val) { return 0xFF00 + val; }

    /***
     * Instructions
     ***/

    // 8-bit loads
    void LD(uint8_t &reg, uint16_t addr) { reg = read(addr); }
    void LD(uint8_t &reg1, uint8_t reg2) { reg1 = reg2; }
    void LD(uint16_t addr1, uint16_t addr2) { write(addr1, read(addr2)); }
    void LD(uint16_t addr, uint8_t reg) { write(addr, reg); }

    // 16-bit loads
    void LD16(uint16_t &reg, uint16_t addr) { reg = read16(addr); }
    void LD16(uint8_t &reg_h, uint8_t &reg_l, uint16_t addr) {
        LD16_val(reg_h, reg_l, read16(addr));
    }
    void LD16_val(uint16_t addr, uint16_t val) { write16(addr, val); }
    void LD16_val(uint16_t &reg, uint8_t val_h, uint8_t val_l) {
        reg = reg16(val_h, val_l);
    }
    void LD16_val(uint8_t &reg_h, uint8_t &reg_l, uint16_t val) {
        reg_h = (val >> 8) & 0xFF;
        reg_l = val & 0xFF;
    }

    void LDHL() {
        LD16_val(H, L, SP + (int8_t) immediate());
        F.zero = 0;
        F.subtract = 0;
        // TODO: Handle carry flags
    }

    void PUSH(uint8_t h, uint8_t l) {
        push(h);
        push(l);
    }
    void POP(uint8_t &h, uint8_t &l) {
        l = pop();
        h = pop();
    }

    // 8-bit ALU
    void update_flags() {
        F.zero = (A == 0);
        // TODO: Handle carry flags
    }

    void ADD(uint8_t val) {
        A += val;
        update_flags();
    }
    void ADC(uint8_t val) { ADD(val + F.carry); }

    void SUB(uint8_t val) { ADD((uint8_t)(val ^ 0xFF)); }
    void SBC(uint8_t val) { ADC((uint8_t)(val ^ 0xFF)); }

    void AND(uint8_t val) {
        A &= val;
        update_flags();
    }
    void OR(uint8_t val) {
        A |= val;
        update_flags();
    }
    void XOR(uint8_t val) {
        A ^= val;
        update_flags();
    }

    void CP(uint8_t val) {
        uint8_t res = A + (val ^ 0xFF);
        // TODO: Update flags
    }

    void INC(uint8_t &reg) {
        ++reg;
        // TODO: Update flags
    }
    void INC_addr(uint16_t addr) { write(addr, read(addr) + 1); }

    void DEC(uint8_t &reg) {
        --reg;
        // TODO: Update flags
    }
    void DEC_addr(uint16_t addr) { write(addr, read(addr) - 1); }

    // 16-bit ALU
    void ADD_HL(uint16_t val) { LD16_val(H, L, HL() + val); }
    void ADD_SP(uint16_t addr) { SP += (int8_t) read(addr); }

    void INC(uint8_t &h, uint8_t &l) { LD16_val(h, l, reg16(h, l) + 1); }
    void INC(uint16_t &reg) { reg += 1; }

    void DEC(uint8_t &h, uint8_t &l) { LD16_val(h, l, reg16(h, l) - 1); }
    void DEC(uint16_t &reg) { reg -= 1; }

    // Miscellaneous
    void SWAP(uint8_t &reg) {
        reg = ((reg << 4) & 0xF0) | ((reg >> 4) & 0x0F);
        // TODO: Update flags
    }
    void SWAP_addr(uint16_t addr) {
        uint8_t val = read(addr);
        SWAP(val);
        write(addr, val);
    }

    void exec_instruction(uint8_t opcode) {
        switch (opcode) {
        case 0x00: /* NOP */
            break;

            // 8-bit loads
        case 0x06: LD(B, immediate()); break;
        case 0x0E: LD(C, immediate()); break;
        case 0x16: LD(D, immediate()); break;
        case 0x1E: LD(E, immediate()); break;
        case 0x26: LD(H, immediate()); break;
        case 0x2E: LD(L, immediate()); break;
        case 0x36: LD(HL(), immediate()); break;
        case 0x3E: LD(A, immediate()); break;

        case 0x40: LD(B, B); break;
        case 0x48: LD(C, B); break;
        case 0x41: LD(B, C); break;
        case 0x49: LD(C, C); break;
        case 0x42: LD(B, D); break;
        case 0x4A: LD(C, D); break;
        case 0x43: LD(B, E); break;
        case 0x4B: LD(C, E); break;
        case 0x44: LD(B, H); break;
        case 0x4C: LD(C, H); break;
        case 0x45: LD(B, L); break;
        case 0x4D: LD(C, L); break;
        case 0x46: LD(B, HL()); break;
        case 0x4E: LD(C, HL()); break;
        case 0x47: LD(B, A); break;
        case 0x4F: LD(C, A); break;

        case 0x50: LD(D, B); break;
        case 0x58: LD(E, B); break;
        case 0x51: LD(D, C); break;
        case 0x59: LD(E, C); break;
        case 0x52: LD(D, D); break;
        case 0x5A: LD(E, D); break;
        case 0x53: LD(D, E); break;
        case 0x5B: LD(E, E); break;
        case 0x54: LD(D, H); break;
        case 0x5C: LD(E, H); break;
        case 0x55: LD(D, L); break;
        case 0x5D: LD(E, L); break;
        case 0x56: LD(D, HL()); break;
        case 0x5E: LD(E, HL()); break;
        case 0x57: LD(D, A); break;
        case 0x5F: LD(E, A); break;

        case 0x60: LD(H, B); break;
        case 0x68: LD(L, B); break;
        case 0x61: LD(H, C); break;
        case 0x69: LD(L, C); break;
        case 0x62: LD(H, D); break;
        case 0x6A: LD(L, D); break;
        case 0x63: LD(H, E); break;
        case 0x6B: LD(L, E); break;
        case 0x64: LD(H, H); break;
        case 0x6C: LD(L, H); break;
        case 0x65: LD(H, L); break;
        case 0x6D: LD(L, L); break;
        case 0x66: LD(H, HL()); break;
        case 0x6E: LD(L, HL()); break;
        case 0x67: LD(H, A); break;
        case 0x6F: LD(L, A); break;

        case 0x70: LD(HL(), B); break;
        case 0x71: LD(HL(), C); break;
        case 0x72: LD(HL(), D); break;
        case 0x73: LD(HL(), E); break;
        case 0x74: LD(HL(), H); break;
        case 0x75: LD(HL(), L); break;
        case 0x77: LD(HL(), A); break;

        case 0x02: LD(BC(), A); break;
        case 0x12: LD(DE(), A); break;

        case 0xEA: LD(indirect(), A); break;

        case 0x32:
            LD(HL(), A);
            DEC(H, L);
            break;
        case 0x22:
            LD(HL(), A);
            INC(H, L);
            break;

        case 0x3A:
            LD(A, HL());
            DEC(H, L);
            break;
        case 0x2A:
            LD(A, HL());
            INC(H, L);
            break;

        case 0xE0: LD(indirect_h(immediate()), A); break;
        case 0xF0: LD(A, indirect_h(immediate())); break;

        case 0x78: LD(A, B); break;
        case 0x79: LD(A, C); break;
        case 0x7A: LD(A, D); break;
        case 0x7B: LD(A, E); break;
        case 0x7C: LD(A, H); break;
        case 0x7D: LD(A, L); break;
        case 0x7E: LD(A, HL()); break;
        case 0x7F: LD(A, A); break;
        case 0x0A: LD(A, BC()); break;
        case 0x1A: LD(A, DE()); break;
        case 0xFA: LD(A, indirect()); break;

        case 0xE2: LD(indirect_h(C), A); break;
        case 0xF2:
            LD(A, indirect_h(C));
            break;

            // 16-bit loads
        case 0x01: LD16(B, C, immediate()); break;
        case 0x11: LD16(D, E, immediate()); break;
        case 0x21: LD16(H, L, immediate()); break;
        case 0x31: LD16(SP, immediate()); break;

        case 0xF8: LDHL(); break;
        case 0xF9: LD16_val(SP, H, L); break;

        case 0x08: LD16_val(indirect(), SP); break;

        case 0xC5: PUSH(B, C); break;
        case 0xD5: PUSH(D, E); break;
        case 0xE5: PUSH(H, L); break;
        case 0xF5: PUSH(A, F.bits); break;

        case 0xC1: POP(B, C); break;
        case 0xD1: POP(D, E); break;
        case 0xE1: POP(H, L); break;
        case 0xF1:
            POP(A, F.bits);
            break;

            // 8-bit ALU
        case 0x80: ADD(B); break;
        case 0x88: ADC(B); break;
        case 0x81: ADD(C); break;
        case 0x89: ADC(C); break;
        case 0x82: ADD(D); break;
        case 0x8A: ADC(D); break;
        case 0x83: ADD(E); break;
        case 0x8B: ADC(E); break;
        case 0x84: ADD(H); break;
        case 0x8C: ADC(H); break;
        case 0x85: ADD(L); break;
        case 0x8D: ADC(L); break;
        case 0x86: ADD(read(HL())); break;
        case 0x8E: ADC(read(HL())); break;
        case 0x87: ADD(A); break;
        case 0x8F: ADC(A); break;
        case 0xC6: ADD(read(immediate())); break;
        case 0xCE: ADC(read(immediate())); break;

        case 0x90: SUB(B); break;
        case 0x98: SBC(B); break;
        case 0x91: SUB(C); break;
        case 0x99: SBC(C); break;
        case 0x92: SUB(D); break;
        case 0x9A: SBC(D); break;
        case 0x93: SUB(E); break;
        case 0x9B: SBC(E); break;
        case 0x94: SUB(H); break;
        case 0x9C: SBC(H); break;
        case 0x95: SUB(L); break;
        case 0x9D: SBC(L); break;
        case 0x96: SUB(read(HL())); break;
        case 0x9E: SBC(read(HL())); break;
        case 0x97: SUB(A); break;
        case 0x9F: SBC(A); break;
        case 0xD6: SUB(read(immediate())); break;
        case 0xDE: SBC(read(immediate())); break;

        case 0xA0: AND(B); break;
        case 0xB0: OR(B); break;
        case 0xA1: AND(C); break;
        case 0xB1: OR(C); break;
        case 0xA2: AND(D); break;
        case 0xB2: OR(D); break;
        case 0xA3: AND(E); break;
        case 0xB3: OR(E); break;
        case 0xA4: AND(H); break;
        case 0xB4: OR(H); break;
        case 0xA5: AND(L); break;
        case 0xB5: OR(L); break;
        case 0xA6: AND(read(HL())); break;
        case 0xB6: OR(read(HL())); break;
        case 0xA7: AND(A); break;
        case 0xB7: OR(A); break;
        case 0xE6: AND(read(immediate())); break;
        case 0xF6: OR(read(immediate())); break;

        case 0xA8: XOR(B); break;
        case 0xB8: CP(B); break;
        case 0xA9: XOR(C); break;
        case 0xB9: CP(C); break;
        case 0xAA: XOR(D); break;
        case 0xBA: CP(D); break;
        case 0xAB: XOR(E); break;
        case 0xBB: CP(E); break;
        case 0xAC: XOR(H); break;
        case 0xBC: CP(H); break;
        case 0xAD: XOR(L); break;
        case 0xBD: CP(L); break;
        case 0xAE: XOR(read(HL())); break;
        case 0xBE: CP(read(HL())); break;
        case 0xAF: XOR(A); break;
        case 0xBF: CP(A); break;
        case 0xEE: XOR(read(immediate())); break;
        case 0xFE: CP(read(immediate())); break;

        case 0x04: INC(B); break;
        case 0x05: DEC(B); break;
        case 0x0C: INC(C); break;
        case 0x0D: DEC(C); break;
        case 0x14: INC(D); break;
        case 0x15: DEC(D); break;
        case 0x1C: INC(E); break;
        case 0x1D: DEC(E); break;
        case 0x24: INC(H); break;
        case 0x25: DEC(H); break;
        case 0x2C: INC(L); break;
        case 0x2D: DEC(L); break;
        case 0x34: INC_addr(HL()); break;
        case 0x35: DEC_addr(HL()); break;
        case 0x3C: INC(A); break;
        case 0x3D:
            DEC(A);
            break;

            // 16-bit ALU
        case 0x09: ADD_HL(reg16(B, C)); break;
        case 0x19: ADD_HL(reg16(D, E)); break;
        case 0x29: ADD_HL(reg16(H, L)); break;
        case 0x39: ADD_HL(SP); break;

        case 0xE8: ADD_SP(immediate()); break;

        case 0x03: INC(B, C); break;
        case 0x0B: DEC(B, C); break;
        case 0x13: INC(D, E); break;
        case 0x1B: DEC(D, E); break;
        case 0x23: INC(H, L); break;
        case 0x2B: DEC(H, L); break;
        case 0x33: INC(SP); break;
        case 0x3B:
            DEC(SP);
            break;

            // Miscellaneous
        case 0x27: DAA(); break;
        case 0x2F: CPL(); break;
        case 0x37: SCF(); break;
        case 0x3F: CCF(); break;
        case 0x76: HALT(); break;
        case 0x10:
            next();
            STOP();
            break;
        case 0xF3: DI(); break;
        case 0xFB:
            EI();
            break;

            // Rotates & Shifts
        case 0x07: RLCA(); break;
        case 0x0F: RRCA(); break;
        case 0x17: RLA(); break;
        case 0x1F: RRA(); break;

        case 0xCB:
            switch (next()) {
                // Miscellaneous
            case 0x30: SWAP(B); break;
            case 0x31: SWAP(C); break;
            case 0x32: SWAP(D); break;
            case 0x33: SWAP(E); break;
            case 0x34: SWAP(H); break;
            case 0x35: SWAP(L); break;
            case 0x36: SWAP_addr(HL()); break;
            case 0x37:
                SWAP(A);
                break;

                // Rotates & Shifts
            case 0x00: RLC(B); break;
            case 0x01: RLC(C); break;
            case 0x02: RLC(D); break;
            case 0x03: RLC(E); break;
            case 0x04: RLC(H); break;
            case 0x05: RLC(L); break;
            case 0x06: RLC_addr(HL()); break;
            case 0x07: RLC(A); break;

            case 0x08: RRC(B); break;
            case 0x09: RRC(C); break;
            case 0x0A: RRC(D); break;
            case 0x0B: RRC(E); break;
            case 0x0C: RRC(H); break;
            case 0x0D: RRC(L); break;
            case 0x0E: RRC_addr(HL()); break;
            case 0x0F: RRC(A); break;

            case 0x10: RL(B); break;
            case 0x11: RL(C); break;
            case 0x12: RL(D); break;
            case 0x13: RL(E); break;
            case 0x14: RL(H); break;
            case 0x15: RL(L); break;
            case 0x16: RL_addr(HL()); break;
            case 0x17: RL(A); break;

            case 0x18: RR(B); break;
            case 0x19: RR(C); break;
            case 0x1A: RR(D); break;
            case 0x1B: RR(E); break;
            case 0x1C: RR(H); break;
            case 0x1D: RR(L); break;
            case 0x1E: RR_addr(HL()); break;
            case 0x1F: RR(A); break;

            case 0x20: SLA(B); break;
            case 0x21: SLA(C); break;
            case 0x22: SLA(D); break;
            case 0x23: SLA(E); break;
            case 0x24: SLA(H); break;
            case 0x25: SLA(L); break;
            case 0x26: SLA_addr(HL()); break;
            case 0x27: SLA(A); break;

            case 0x28: SRA(B); break;
            case 0x29: SRA(C); break;
            case 0x2A: SRA(D); break;
            case 0x2B: SRA(E); break;
            case 0x2C: SRA(H); break;
            case 0x2D: SRA(L); break;
            case 0x2E: SRA_addr(HL()); break;
            case 0x2F: SRA(A); break;

            case 0x38: SRL(B); break;
            case 0x39: SRL(C); break;
            case 0x3A: SRL(D); break;
            case 0x3B: SRL(E); break;
            case 0x3C: SRL(H); break;
            case 0x3D: SRL(L); break;
            case 0x3E: SRL_addr(HL()); break;
            case 0x3F:
                SRL(A);
                break;

                // Bit Operations
            }
            break;
        }
    }
};

#endif // CPU_H
