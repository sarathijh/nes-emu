#ifndef UNKNOWN_ROM_EXCEPTION_HPP
#define UNKNOWN_ROM_EXCEPTION_HPP

#include <stdexcept>

class UnknownRomError : public std::runtime_error {
public:
    explicit UnknownRomError(const char *message);
};

#endif // UNKNOWN_ROM_EXCEPTION_HPP
