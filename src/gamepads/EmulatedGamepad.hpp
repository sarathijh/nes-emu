#ifndef GameController_HPP
#define GameController_HPP

#include <QObject>
#include <QList>
#include <QMap>
#include <QGamepadManager>

class EmulatedGamepad : public QObject {
    Q_OBJECT;

private:
    bool shouldStop = false;
    QList<QString> buttons;
    QList<QString> joysticks;
    QMap<int, QString> keyToButtonMap;
    QMap<QGamepadManager::GamepadButton, QString> gamepadButtonToButtonMap;
    
public:
    EmulatedGamepad();
    
public:
    void stop() { shouldStop = true; }

protected:
    void setButtons(const QList<QString> &buttons);
    void setJoysticks(const QList<QString> &joysticks);
    void setKeyToButtonMap(const QMap<int, QString> &map);
    void setGamepadButtonToButtonMap(const QMap<QGamepadManager::GamepadButton, QString> &map);
    
signals:
    void buttonPressed(QString button);
    void buttonReleased(QString button);

public slots:
    void startListening();
    void onHardwareKeyPressed(int key);
    void onHardwareKeyReleased(int key);
    void onGamepadButtonPressed(QGamepadManager::GamepadButton button);
    void onGamepadButtonReleased(QGamepadManager::GamepadButton button);
};

#endif // GameController_HPP
