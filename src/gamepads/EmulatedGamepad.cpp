#include "EmulatedGamepad.hpp"
#include <SDL.h>
#include <QDebug>

EmulatedGamepad::EmulatedGamepad() {
    
}

void EmulatedGamepad::startListening() {
    SDL_Init(SDL_INIT_JOYSTICK);
    
    if (SDL_NumJoysticks() > 0) {
        SDL_JoystickEventState(SDL_ENABLE);
        SDL_Joystick *joystick = SDL_JoystickOpen(0);
        auto guid = SDL_JoystickGetGUID(joystick);
        char guidString[1024];
        SDL_JoystickGetGUIDString(guid, guidString, sizeof(guidString));
        
        auto name = SDL_JoystickName(joystick);

        int num_axes = SDL_JoystickNumAxes(joystick);
        int num_buttons = SDL_JoystickNumButtons(joystick);
        int num_hats = SDL_JoystickNumHats(joystick);
        int num_balls = SDL_JoystickNumBalls(joystick);

        printf("Joystick connected: %s \"%s\" axes:%d buttons:%d hats:%d balls:%d\n",
               guidString, name,
               num_axes, num_buttons, num_hats, num_balls);
        
        while (!shouldStop) {
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_JOYBUTTONDOWN:
                        switch (event.jbutton.button) {
                            case 0: onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonY); break;
                            case 1: onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonB); break;
                            case 2: onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonA); break;
                            case 3: onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonX); break;
                            case 8: onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonSelect); break;
                            case 9: onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonStart); break;
                        }
                        break;
                    case SDL_JOYBUTTONUP:
                        switch (event.jbutton.button) {
                            case 0: onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonY); break;
                            case 1: onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonB); break;
                            case 2: onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonA); break;
                            case 3: onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonX); break;
                            case 8: onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonSelect); break;
                            case 9: onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonStart); break;
                        }
                        break;
                    case SDL_JOYHATMOTION:
                        if (event.jhat.value & SDL_HAT_RIGHT) {
                            onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonRight);
                        } else {
                            onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonRight);
                        }
                        if (event.jhat.value & SDL_HAT_LEFT) {
                            onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonLeft);
                        } else {
                            onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonLeft);
                        }
                        if (event.jhat.value & SDL_HAT_UP) {
                            onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonUp);
                        } else {
                            onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonUp);
                        }
                        if (event.jhat.value & SDL_HAT_DOWN) {
                            onGamepadButtonPressed(QGamepadManager::GamepadButton::ButtonDown);
                        } else {
                            onGamepadButtonReleased(QGamepadManager::GamepadButton::ButtonDown);
                        }
                        break;
                }
            }
        }
    }
    
    SDL_Quit();
}

void EmulatedGamepad::setButtons(const QList<QString> &buttons) {
    this->buttons = buttons;
}

void EmulatedGamepad::setJoysticks(const QList<QString> &joysticks) {
    this->joysticks = joysticks;
}

void EmulatedGamepad::setKeyToButtonMap(const QMap<int, QString> &map) {
    this->keyToButtonMap = map;
}

void EmulatedGamepad::setGamepadButtonToButtonMap(const QMap<QGamepadManager::GamepadButton, QString> &map) {
    this->gamepadButtonToButtonMap = map;
}

void EmulatedGamepad::onHardwareKeyPressed(int key) {
    if (keyToButtonMap.contains(key)) {
        emit buttonPressed(keyToButtonMap.find(key).value());
    }
}

void EmulatedGamepad::onHardwareKeyReleased(int key) {
    if (keyToButtonMap.contains(key)) {
        emit buttonReleased(keyToButtonMap.find(key).value());
    }
}

void EmulatedGamepad::onGamepadButtonPressed(QGamepadManager::GamepadButton button) {
    if (gamepadButtonToButtonMap.contains(button)) {
        emit buttonPressed(gamepadButtonToButtonMap.find(button).value());
    }
}

void EmulatedGamepad::onGamepadButtonReleased(QGamepadManager::GamepadButton button) {
    if (gamepadButtonToButtonMap.contains(button)) {
        emit buttonReleased(gamepadButtonToButtonMap.find(button).value());
    }
}
