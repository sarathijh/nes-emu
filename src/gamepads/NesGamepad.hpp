#ifndef NesController_HPP
#define NesController_HPP

#include "EmulatedGamepad.hpp"

class NesGamepad : public EmulatedGamepad {
public:
    NesGamepad() {
        setButtons({ "A", "B", "Select", "Start", "Up", "Down", "Left", "Right", "_SpeedUp" });
        setJoysticks({ "DirectionalPad" });
        setKeyToButtonMap(
                {
                        {Qt::Key_X, "A"},
                        {Qt::Key_Z, "B"},
                        {Qt::Key_Shift, "Select"},
                        {Qt::Key_Enter, "Start"},
                        {Qt::Key_Return, "Start"},
                        {Qt::Key_Up, "Up"},
                        {Qt::Key_Down, "Down"},
                        {Qt::Key_Left, "Left"},
                        {Qt::Key_Right, "Right"},
                        {Qt::Key_Space, "_SpeedUp"},
                });
        setGamepadButtonToButtonMap(
                {
                        {QGamepadManager::GamepadButton::ButtonA, "A"},
                        {QGamepadManager::GamepadButton::ButtonB, "B"},
                        {QGamepadManager::GamepadButton::ButtonSelect, "Select"},
                        {QGamepadManager::GamepadButton::ButtonStart, "Start"},
                        {QGamepadManager::GamepadButton::ButtonUp, "Up"},
                        {QGamepadManager::GamepadButton::ButtonDown, "Down"},
                        {QGamepadManager::GamepadButton::ButtonLeft, "Left"},
                        {QGamepadManager::GamepadButton::ButtonRight, "Right"},
                });
    }
};

#endif // NesController_HPP
