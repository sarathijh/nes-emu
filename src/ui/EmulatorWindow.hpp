#ifndef NESEMU_EMULATORWINDOW_HPP
#define NESEMU_EMULATORWINDOW_HPP

#include "opengl/OpenGLWindow.hpp"

class EmulatorWindow : public OpenGLWindow {
    Q_OBJECT;
    
public:
    EmulatorWindow();

protected:
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;

public:
    void setRenderer(OpenGLRenderer *renderer) override;

signals:
    void keyPressed(int keyCode);
    void keyReleased(int keyCode);
};

#endif // NESEMU_EMULATORWINDOW_HPP
