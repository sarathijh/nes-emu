#include "EmulatorRenderer.hpp"

#include <QResizeEvent>

EmulatorRenderer::EmulatorRenderer() : shaderProgram(nullptr) {
    myTimer.start();
}

void EmulatorRenderer::setFrameSize(int width, int height) {
    _frameWidth = width;
    _frameHeight = height;
}

int EmulatorRenderer::frameWidth() { return _frameWidth; }
int EmulatorRenderer::frameHeight() { return _frameHeight; }

void EmulatorRenderer::initializeGL() {
    linkAndBindShaderProgram();
    getShaderAttributeAndUniformLocations();
    createTexture();
    initializeVerticesAndTexCoords();
}

void EmulatorRenderer::linkAndBindShaderProgram() {
    shaderProgram = new QOpenGLShaderProgram(this);
    shaderProgram->addCacheableShaderFromSourceFile(
        QOpenGLShader::Vertex, ":/default-vert.glsl");
    shaderProgram->addCacheableShaderFromSourceFile(
        QOpenGLShader::Fragment, ":/default-frag.glsl");
    shaderProgram->link();
    shaderProgram->bind();
}

void EmulatorRenderer::getShaderAttributeAndUniformLocations() {
    positionAttribute = (GLuint) shaderProgram->attributeLocation("position");
    texCoordAttribute = (GLuint) shaderProgram->attributeLocation("texCoord");
    textureUniform = (GLuint) shaderProgram->uniformLocation("source");
}

void EmulatorRenderer::createTexture() {
    glGenTextures(1, &textureId);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    float colour[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, colour);

    glUniform1i(textureUniform, 0);
}

void EmulatorRenderer::initializeVerticesAndTexCoords() {
    static const GLfloat texCoords[] = {
        0,
        1,
        1,
        1,
        0,
        0,
        1,
        0,
    };

    glVertexAttribPointer(
        texCoordAttribute, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    glEnableVertexAttribArray(texCoordAttribute);
    glEnableVertexAttribArray(positionAttribute);
}

void EmulatorRenderer::renderGL() {
    const GLfloat vertices[] = {
        left,
        bottom,
        right,
        bottom,
        left,
        top,
        right,
        top,
    };
    glVertexAttribPointer(
        positionAttribute, 2, GL_FLOAT, GL_FALSE, 0, vertices);

    glUniform1f(shaderProgram->uniformLocation("time"), myTimer.elapsed());

    glClear(GL_COLOR_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void EmulatorRenderer::setTextureData(
    unsigned char *data, int width, int height) {
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGB,
        width,
        height,
        0,
        GL_BGRA,
        GL_UNSIGNED_BYTE,
        data);
    render();
}

void EmulatorRenderer::onResize(int width, int height) {
    GeomUtil::Size windowSize = {(float) width, (float) height};
    GeomUtil::Size scaledSize = GeomUtil::aspectFit(
        {(float) _frameWidth, (float) _frameHeight}, windowSize);
    adjustEmulatorViewForSize(scaledSize, windowSize);
}

void EmulatorRenderer::adjustEmulatorViewForSize(
    const GeomUtil::Size &renderSize, const GeomUtil::Size &windowSize) {
    right = renderSize.width / windowSize.width;
    left = -right;

    top = renderSize.height / windowSize.height;
    bottom = -top;
}
