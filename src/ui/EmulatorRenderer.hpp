#ifndef EMULATOR_GL_WINDOW_HPP
#define EMULATOR_GL_WINDOW_HPP

#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include "../util/GeomUtil.hpp"
#include <QTime>
#include "opengl/OpenGLWindow.hpp"

class EmulatorRenderer : public OpenGLRenderer {
private:
    QOpenGLShaderProgram *shaderProgram;
    GLuint textureId;

    GLuint positionAttribute;
    GLuint texCoordAttribute;
    GLuint textureUniform;

    int _frameWidth = 256;
    int _frameHeight = 240;

    GLfloat left = -1;
    GLfloat top = 1;
    GLfloat right = 1;
    GLfloat bottom = -1;
    
    QTime myTimer;

public:
    EmulatorRenderer();

public:
    void setFrameSize(int width, int height);
    int frameWidth();
    int frameHeight();

    void initializeGL() override;
    void linkAndBindShaderProgram();
    void getShaderAttributeAndUniformLocations();
    void createTexture();
    void initializeVerticesAndTexCoords();

    void renderGL() override;

private:
    void adjustEmulatorViewForSize(const GeomUtil::Size &renderSize, const GeomUtil::Size &windowSize);

public slots:
    void setTextureData(unsigned char *data, int width, int height);
    void onResize(int width, int height) override;
};

#endif // EMULATOR_GL_WINDOW_HPP
