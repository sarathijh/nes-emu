#include "CocoaBridge.h"
#import <Cocoa/Cocoa.h>
#import <QCoreApplication>
#import <QWindow>

void CocoaBridge::setAllowsAutomaticWindowTabbing(bool flag) {
    [NSWindow setAllowsAutomaticWindowTabbing: flag];
}

void CocoaBridge::configureEmulatorWindow(QWindow *window) {
    QCoreApplication::setAttribute( Qt::AA_DontCreateNativeWidgetSiblings );
    auto nsView = (__bridge NSView *)reinterpret_cast<void*>(window->winId());
    NSWindow *nsWindow = [nsView window];
    nsWindow.animationBehavior = NSWindowAnimationBehaviorAlertPanel;
    nsWindow.movableByWindowBackground = YES;
}

void CocoaBridge::makeBackgroundTranslucent(QWindow *window) {
    auto *nsView = (__bridge NSView *)reinterpret_cast<void*>(window->winId());
    NSWindow *nsWindow = [nsView window];

    NSRect windowFrame = [NSWindow frameRectForContentRect:[[nsWindow contentView] bounds] styleMask:[nsWindow styleMask]];
    NSRect contentBounds = [[nsWindow contentView] bounds];

    NSRect titleBarRect = NSMakeRect(0, 0, nsView.bounds.size.width, windowFrame.size.height - contentBounds.size.height);
    
    auto *visualEffectView = [[NSVisualEffectView alloc] initWithFrame: titleBarRect];
    visualEffectView.material = NSVisualEffectMaterialTitlebar;
    visualEffectView.blendingMode = NSVisualEffectBlendingModeWithinWindow;
    visualEffectView.autoresizingMask = NSViewWidthSizable;
    
    nsWindow.titlebarAppearsTransparent = true;
    nsWindow.styleMask |= NSWindowStyleMaskFullSizeContentView;
    [nsView addSubview:visualEffectView positioned:NSWindowAbove relativeTo:nil];
    //nsWindow.contentView = visualEffectView;
    //[visualEffectView addSubview:nsView positioned:NSWindowAbove relativeTo:nil];
}
