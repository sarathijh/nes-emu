#ifndef COCOABRIDGE_H
#define COCOABRIDGE_H

class QWindow;

class CocoaBridge {
public:
    static void setAllowsAutomaticWindowTabbing(bool flag);
    static void configureEmulatorWindow(QWindow *window);
    static void makeBackgroundTranslucent(QWindow *window);

};

#endif // COCOABRIDGE_H
