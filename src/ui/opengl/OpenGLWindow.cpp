#include "OpenGLWindow.hpp"

OpenGLWindow::OpenGLWindow(QWindow *parent)
    : QWindow(parent), glRenderer(nullptr) {
    setSurfaceType(QWindow::OpenGLSurface);

    renderThread = new QThread;
    renderThread->start();
}

OpenGLWindow::~OpenGLWindow() {
    renderThread->quit();
    renderThread->wait();
}

void OpenGLWindow::setRenderer(OpenGLRenderer *renderer) {
    glRenderer = renderer;

    renderer->moveToThread(renderThread);
    renderer->setSurface(this);
}

void OpenGLWindow::exposeEvent(QExposeEvent *event) {
    Q_UNUSED(event);

    if (isExposed()) {
        glRenderer->setEnabled(true);
        QMetaObject::invokeMethod(glRenderer, "render", Qt::QueuedConnection);
    } else {
        glRenderer->setEnabled(false);
    }
}

void OpenGLWindow::resizeEvent(QResizeEvent *ev) {
    QWindow::resizeEvent(ev);
    QMetaObject::invokeMethod(
        glRenderer,
        "onResize",
        Qt::DirectConnection,
        Q_ARG(int, width()),
        Q_ARG(int, height()));
}
