#ifndef OpenGLRenderer_HPP
#define OpenGLRenderer_HPP

#include <QOpenGLFunctions>

class OpenGLRenderer : public QObject, protected QOpenGLFunctions {
    Q_OBJECT;

private:
    bool enabled;
    QOpenGLContext *glContext;
    QSurface *surface;

public:
    OpenGLRenderer();

protected:
    virtual void initializeGL() = 0;
    virtual void renderGL() = 0;

public:
    void setEnabled(bool enabled);
    void setSurface(QSurface *surface);

public slots:
    void render();
    virtual void onResize(int width, int height) {}

signals:
    void onInitialized();
};

#endif // OpenGLRenderer_HPP
