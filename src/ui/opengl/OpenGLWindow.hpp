#ifndef OPEN_GL_WINDOW_HPP
#define OPEN_GL_WINDOW_HPP

#include "OpenGLRenderer.hpp"

#include <QThread>
#include <QWindow>

class OpenGLWindow : public QWindow {
    Q_OBJECT;

private:
    QThread *renderThread;
    OpenGLRenderer *glRenderer;

public:
    explicit OpenGLWindow(QWindow *parent = nullptr);
    ~OpenGLWindow() override;

public:
    virtual void setRenderer(OpenGLRenderer *renderer);

protected:
    void exposeEvent(QExposeEvent *event) override;
    void resizeEvent(QResizeEvent *ev) override;
};

#endif // OPEN_GL_WINDOW_HPP
