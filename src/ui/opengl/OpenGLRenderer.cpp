#include "OpenGLRenderer.hpp"

OpenGLRenderer::OpenGLRenderer():
    glContext(nullptr),
    surface(nullptr)
{

}

void OpenGLRenderer::setEnabled(bool enabled) {
    this->enabled = enabled;
}

void OpenGLRenderer::setSurface(QSurface *surface) {
    this->surface = surface;
}

void OpenGLRenderer::render() {
    if (!enabled) {
        return;
    }

    bool needsInitialize = false;

    if (!glContext) {
        glContext = new QOpenGLContext();
        glContext->create();

        needsInitialize = true;
    }

    glContext->makeCurrent(surface);

    if (needsInitialize) {
        initializeOpenGLFunctions();
        initializeGL();
        emit onInitialized();
    }

    renderGL();

    glContext->swapBuffers(surface);
}
