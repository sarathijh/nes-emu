#include "EmulatorWindow.hpp"

#include "EmulatorRenderer.hpp"

#include <QKeyEvent>

EmulatorWindow::EmulatorWindow() {
    setFlag(Qt::WindowFullscreenButtonHint, true);
}

void EmulatorWindow::keyPressEvent(QKeyEvent *ev) {
    emit keyPressed(ev->key());
}

void EmulatorWindow::keyReleaseEvent(QKeyEvent *ev) {
    emit keyReleased(ev->key());
}

void EmulatorWindow::setRenderer(OpenGLRenderer *renderer) {
    OpenGLWindow::setRenderer(renderer);

    auto emulator_renderer = dynamic_cast<EmulatorRenderer *>(renderer);
    if (emulator_renderer != nullptr) {
        setMinimumWidth(emulator_renderer->frameWidth());
        setMinimumHeight(emulator_renderer->frameHeight());
        resize(minimumWidth() * 2, minimumHeight() * 2);
    }
}
