#ifndef AUDIOOUTPUT_H
#define AUDIOOUTPUT_H

#include <math.h>

#include <QAudioOutput>
#include <QByteArray>
#include <QComboBox>
#include <QIODevice>
#include <QLabel>
#include <QMainWindow>
#include <QObject>
#include <QPushButton>
#include <QSlider>
#include <QTimer>
#include <QScopedPointer>

class Generator : public QIODevice
{
Q_OBJECT

public:
    Generator(const QAudioFormat &format);

    void start();
    void stop();

    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *data, qint64 len) override;
    qint64 bytesAvailable() const override;
    
    void addSample(quint8 sample);

private:
    qint64 m_pos = 0;
    QByteArray m_buffer;
};

class EmulatedAudioStream : public QMainWindow
{
Q_OBJECT

public:
    EmulatedAudioStream();

public slots:
    void writeSample(quint8 sample);

private:
    void initializeAudio(const QAudioDeviceInfo &deviceInfo);

private:
    QScopedPointer<Generator> m_generator;
    QScopedPointer<QAudioOutput> m_audioOutput;
};

#endif // AUDIOOUTPUT_H
