#ifndef NES_WRAPPER_HPP
#define NES_WRAPPER_HPP

#include "EmulatorWrapper.hpp"

class NES;

class NesWrapper : public EmulatorWrapper {
private:
    static const int FRAME_WIDTH = 256;
    static const int FRAME_HEIGHT = 240;

private:
    NES *nes;

public:
    explicit NesWrapper(const QString &filename);

public:
    FrameFormat getFrameFormat() override;

protected:
    void powerOn() override;
    void execFrame() override;
    
private:
    int nesButtonFromEmulatedButton(const QString &emulatedButton);

public slots:
    void onGamepadButtonPressed(const QString &button) override;
    void onGamepadButtonReleased(const QString &button) override;
    void setState(unsigned char *state) override;
    unsigned char* getState() override;
};

#endif // NES_WRAPPER_HPP
