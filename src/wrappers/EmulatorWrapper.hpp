#ifndef EMULATOR_WRAPPER_HPP
#define EMULATOR_WRAPPER_HPP

#include "../gamepads/EmulatedGamepad.hpp"
#include <cstdint>
#include <chrono>

class EmulatorWrapper : public QObject {
    Q_OBJECT;
    
public:
    struct FrameFormat {
        int width;
        int height;
        float pixelAspectRatio;
    };

private:
    EmulatedGamepad *gamepad = nullptr;
    bool isFrameLoopRunning = false;
    float targetFrameRate = 1 / 60.f;

public:
    void setTargetFps(float fps);
    virtual FrameFormat getFrameFormat() = 0;

protected:
    virtual void powerOn() = 0;
    virtual void execFrame() = 0;

private:
    void startThrottledExecutionLoop();
    void waitForTargetFrameRate(const std::chrono::high_resolution_clock::time_point &timeAtFrameStart);
    void startTimedExecutionLoop();
    
signals:
    void frameReady(unsigned char *data, int width, int height);
    void audioSampleReady(unsigned char sample);

public slots:
    void startEmulation();
    void stopEmulation();
    virtual void setState(unsigned char *state) = 0;
    virtual unsigned char* getState() = 0;
    
public slots:
    virtual void onGamepadButtonPressed(const QString &button) = 0;
    virtual void onGamepadButtonReleased(const QString &button) = 0;
};

#endif // EMULATOR_WRAPPER_HPP
