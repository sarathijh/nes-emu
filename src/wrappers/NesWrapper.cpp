#include "NesWrapper.hpp"

#include <nes/nes.hpp>

NesWrapper::NesWrapper(const QString &filename) {
    setTargetFps(60);

    nes = new NES(filename.toUtf8().data());

    nes->frame_handler = [this](unsigned *pixels) {
        emit frameReady((unsigned char *)pixels, FRAME_WIDTH, FRAME_HEIGHT);
    };

    nes->audio_handler = [this](uint8_t sample) {
        emit audioSampleReady((unsigned char)((float)sample / 15.0f * 255));
    };
}

EmulatorWrapper::FrameFormat NesWrapper::getFrameFormat() {
    FrameFormat frameFormat;
    frameFormat.width = FRAME_WIDTH;
    frameFormat.height = FRAME_HEIGHT;
    frameFormat.pixelAspectRatio = 292 / 240.0f;
    return frameFormat;
}

void NesWrapper::powerOn() { nes->power_on(); }

void NesWrapper::execFrame() { nes->execute_frame(); }

void NesWrapper::onGamepadButtonPressed(const QString &button) {
    if (button == "_SpeedUp") {
        setTargetFps(600);
    } else {
        nes->set_button(nesButtonFromEmulatedButton(button), true);
    }
}

void NesWrapper::onGamepadButtonReleased(const QString &button) {
    if (button == "_SpeedUp") {
        setTargetFps(60);
    } else {
        nes->set_button(nesButtonFromEmulatedButton(button), false);
    }
}

int NesWrapper::nesButtonFromEmulatedButton(const QString &emulatedButton) {
    static const QList<QString> nesButtonOrder = { "A",     "B",    "Select",
                                                   "Start", "Up",   "Down",
                                                   "Left",  "Right" };
    return nesButtonOrder.indexOf(emulatedButton);
}

void NesWrapper::setState(unsigned char *state) {}

unsigned char *NesWrapper::getState() { return nullptr; }
