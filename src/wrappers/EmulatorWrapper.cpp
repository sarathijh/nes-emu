#include "EmulatorWrapper.hpp"
#include <chrono>
#include <iostream>

using namespace std::chrono;

void EmulatorWrapper::setTargetFps(float fps) {
    targetFrameRate = 1.0f / fps;
}

void EmulatorWrapper::startEmulation() {
    powerOn();
    startThrottledExecutionLoop();
}

void EmulatorWrapper::startThrottledExecutionLoop() {
    isFrameLoopRunning = true;
    while (isFrameLoopRunning) {
        auto timeAtFrameStart = high_resolution_clock::now();
        execFrame();
        waitForTargetFrameRate(timeAtFrameStart);
    }
}

void EmulatorWrapper::waitForTargetFrameRate(const high_resolution_clock::time_point &timeAtFrameStart) {
    static const double microsecondsPerSeconds = 1000000.0;
    
    while (true) {
        auto microsecondsSinceFrameStart = duration_cast<microseconds>(high_resolution_clock::now() - timeAtFrameStart).count();
        double secondsSinceFrameStart = microsecondsSinceFrameStart / microsecondsPerSeconds;
        if (secondsSinceFrameStart >= targetFrameRate) {
            break;
        }
    }
}

void EmulatorWrapper::startTimedExecutionLoop() {
    double frameRate = 0;
    long frame = 0;

    isFrameLoopRunning = true;
    while (isFrameLoopRunning) {
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

        execFrame();
        ++frame;

        std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
        frameRate += (std::chrono::duration_cast<std::chrono::microseconds> (t2 - t1).count() / 1000000.0f);
        std::cout << frame / frameRate << std::endl;
    }
}

void EmulatorWrapper::stopEmulation() {
    isFrameLoopRunning = false;
}
