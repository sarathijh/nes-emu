#include <math.h>
#include "GeomUtil.hpp"

using namespace GeomUtil;

Size GeomUtil::aspectFit(Size actualSize, Size targetSize, bool pixelPerfect /*= false */)
{
    if (targetSize.width != 0 && targetSize.height != 0) {
        const float scalex = targetSize.width / actualSize.width;
        const float scaley = targetSize.height / actualSize.height;

        float scale = fminf(scalex, scaley);

        if (pixelPerfect) {
            scale = floorf(scale);
        }

        actualSize.width *= scale;
        actualSize.height *= scale;
    }

    return actualSize;
}
