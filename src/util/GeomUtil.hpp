#ifndef GeomUtil_HPP
#define GeomUtil_HPP

namespace GeomUtil {
    struct Size {
        float width;
        float height;
    };

    Size aspectFit(Size actualSize, Size targetSize, bool pixelPerfect = false);
};

#endif // GeomUtil_HPP
