#include "EmulatorController.hpp"

#include "UnknownRomException.hpp"
#include "gamepads/NesGamepad.hpp"
#include "ui/EmulatorRenderer.hpp"
#include "wrappers/EmulatorWrapper.hpp"
#include "wrappers/NesWrapper.hpp"

EmulatorController::EmulatorController(const QString &romFilePath)
    : romFileInfo(romFilePath) {
    createEmulatorWrapperAndGamepadFromRomFile();
    createEmulatorRenderer();
    createEmulatedAudioStream();
}

EmulatorController::~EmulatorController() {
    stopEmulatorAndWaitForThreadToFinish();

    delete emulationThread;
    delete emulatorWrapper;
    delete emulatorRenderer;
}

EmulatorRenderer *EmulatorController::renderer() { return emulatorRenderer; }

void EmulatorController::stopEmulatorAndWaitForThreadToFinish() {
    emulatorWrapper->stopEmulation();
    emulatedGamepad->stop();

    emulationThread->quit();
    gamepadThread->quit();

    emulationThread->wait();
    gamepadThread->wait();
}

void EmulatorController::createEmulatorWrapperAndGamepadFromRomFile() {
    QString extension = romFileInfo.suffix();
    if (extension == "nes") {
        emulatorWrapper = new NesWrapper(romFileInfo.filePath());
        emulatedGamepad = new NesGamepad;
        gamepadThread = new QThread();
        emulatedGamepad->moveToThread(gamepadThread);
        gamepadThread->start();
        QMetaObject::invokeMethod(
            emulatedGamepad, "startListening", Qt::QueuedConnection);
    } else {
        throw UnknownRomError(
            ("No emulator core is available to run the given rom: '" +
             romFileInfo.filePath() + "'")
                .toUtf8()
                .data());
    }

    QObject::connect(
        emulatedGamepad,
        &EmulatedGamepad::buttonPressed,
        emulatorWrapper,
        &EmulatorWrapper::onGamepadButtonPressed,
        Qt::DirectConnection);
    
    QObject::connect(
        emulatedGamepad,
        &EmulatedGamepad::buttonReleased,
        emulatorWrapper,
        &EmulatorWrapper::onGamepadButtonReleased,
        Qt::DirectConnection);

    EmulatorWrapper::FrameFormat frameFormat =
        emulatorWrapper->getFrameFormat();
}

void EmulatorController::createEmulatorRenderer() {
    EmulatorWrapper::FrameFormat frameFormat =
        emulatorWrapper->getFrameFormat();

    emulatorRenderer = new EmulatorRenderer;
    emulatorRenderer->setFrameSize(
        (int) ((float) frameFormat.width * frameFormat.pixelAspectRatio),
        frameFormat.height);

    QObject::connect(
        emulatorRenderer,
        &EmulatorRenderer::onInitialized,
        this,
        &EmulatorController::startEmulator);
    
    QObject::connect(
        emulatorWrapper,
        &EmulatorWrapper::frameReady,
        emulatorRenderer,
        &EmulatorRenderer::setTextureData);
}

void EmulatorController::createEmulatedAudioStream() {
    emulatedAudioStream = new EmulatedAudioStream;

    // QObject::connect(emulatorWrapper, &EmulatorWrapper::audioSampleReady,
    // emulatedAudioStream, &EmulatedAudioStream::writeSample);
}

void EmulatorController::startEmulator() {
    emulationThread = new QThread();
    emulatorWrapper->moveToThread(emulationThread);
    emulationThread->start();

    QMetaObject::invokeMethod(
        emulatorWrapper, "startEmulation", Qt::QueuedConnection);
}
