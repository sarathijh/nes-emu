#include "EmulatorApplication.hpp"
#include "EmulatorController.hpp"

#include <QDebug>
#include <iostream>

int main(int argc, char **argv) {
    EmulatorApplication app(argc, argv);
    return EmulatorApplication::exec();
}
