#include "EmulatorApplication.hpp"

#include "EmulatorController.hpp"
#include "ui/EmulatorRenderer.hpp"
#include "ui/EmulatorWindow.hpp"
#include "ui/bridge/CocoaBridge.h"

#include <QDebug>
#include <QtGamepad/QGamepadManager>

EmulatorApplication::EmulatorApplication(int &argc, char **argv)
    : QApplication(argc, argv) {
    CocoaBridge::setAllowsAutomaticWindowTabbing(false);

    qQmlEngine = std::make_unique<QQmlApplicationEngine>(":/main_window.qml");
    QObject *rootObject = qQmlEngine->rootObjects()[0];

    QObject::connect(
        rootObject,
        SIGNAL(fileOpened(const QString &)),
        this,
        SLOT(openRom(const QString &)));

    QWindow *window = qobject_cast<QWindow *>(rootObject);
    CocoaBridge::makeBackgroundTranslucent(window);
}

void EmulatorApplication::openRom(const QString &file) {
    auto controller = new EmulatorController(file);

    emulatorWindow = std::make_shared<EmulatorWindow>();
    emulatorWindow->setTitle(file.split("/").last().split(".").first());
    CocoaBridge::configureEmulatorWindow(emulatorWindow.get());
    emulatorWindow->setRenderer(controller->renderer());
    emulatorWindow->show();
}
