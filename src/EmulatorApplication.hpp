#ifndef EMULATOR_APPLICATION_HPP
#define EMULATOR_APPLICATION_HPP

#include <QApplication>
#include <QQmlApplicationEngine>

class EmulatorWindow;

class EmulatorApplication : public QApplication {
    Q_OBJECT;

private:
    std::shared_ptr<EmulatorWindow> emulatorWindow;
    std::unique_ptr<QQmlApplicationEngine> qQmlEngine;

public:
    EmulatorApplication(int &argc, char **argv);

public slots:
    void openRom(const QString &file);
};

#endif // EMULATOR_APPLICATION_HPP
