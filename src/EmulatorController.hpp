#ifndef EMULATOR_WINDOW_HPP
#define EMULATOR_WINDOW_HPP

#include "audio/EmulatedAudioStream.hpp"
#include "ui/opengl/OpenGLWindow.hpp"

#include <QFileInfo>

class QThread;
class EmulatorRenderer;
class EmulatorWrapper;
class EmulatedGamepad;

class EmulatorController : public QObject {
    Q_OBJECT;

public:
    explicit EmulatorController(const QString &romFilePath);
    ~EmulatorController() override;
    
public:
    EmulatorRenderer* renderer();

private:
    QFileInfo romFileInfo {};

    EmulatorRenderer *emulatorRenderer = nullptr;
    EmulatedAudioStream *emulatedAudioStream = nullptr;

    QThread *emulationThread = nullptr;
    QThread *gamepadThread = nullptr;
    EmulatorWrapper *emulatorWrapper = nullptr;
    EmulatedGamepad *emulatedGamepad = nullptr;

private:
    void createEmulatorWrapperAndGamepadFromRomFile();
    void createEmulatorRenderer();
    void createEmulatedAudioStream();
    void stopEmulatorAndWaitForThreadToFinish();

private slots:
    void startEmulator();
};

#endif // EMULATOR_WINDOW_HPP
