#include "UnknownRomException.hpp"

UnknownRomError::UnknownRomError(const char *message)
    : runtime_error(message) {}
