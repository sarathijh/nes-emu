attribute vec4 position;
attribute vec2 texCoord;

varying vec2 fragTexCoord;

void main() {
    gl_Position = position;
    fragTexCoord = texCoord;
}
