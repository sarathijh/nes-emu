varying vec2 fragTexCoord;

uniform sampler2D source;

void main() {
    gl_FragColor = texture2D(source, fragTexCoord);
}
