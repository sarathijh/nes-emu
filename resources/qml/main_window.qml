import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.3
import Qt.labs.folderlistmodel 2.11
import QtGraphicalEffects 1.0

ApplicationWindow {
    id: appWindow
    visible: true
    title: "Nintendo Entertainment System"
    width: 960
    height: 720
    color: "transparent"
    
    signal fileOpened (string filePath)
    signal filterChanged(string filter)
    
    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            MenuItem {
                text: qsTr("&Open ROM...")
                shortcut: StandardKey.Open
                onTriggered: fileDialog.open()
            }
        }
        Menu {
            title: qsTr("&Edit")
            MenuItem { text: qsTr("Cu&t") }
            MenuItem { text: qsTr("&Copy") }
            MenuItem { text: qsTr("&Paste") }
        }
        Menu {
            title: qsTr("&Filter")
            ExclusiveGroup { id: filter }
            MenuItem {
                text: qsTr("None")
                checkable: true
                checked: true
                exclusiveGroup: filter
                onTriggered: filterChanged("none")
            }
            MenuItem {
                text: qsTr("CRT")
                checkable: true
                exclusiveGroup: filter
                onTriggered: filterChanged("crt")
            }
        }
        Menu {
            title: qsTr("&Help")
            MenuItem { text: qsTr("&About") }
        }
    }
    
    /*statusBar: StatusBar {
        RowLayout {
            anchors.fill: parent
            Label { text: "Read Only" }
        }
    }*/
    
    ScrollView {
        anchors.fill: parent
        
        Item {
            width: appWindow.width - 40
            height: flow.height + 40
            x: 20
            y: 20
            
            GridLayout {
                id: flow
                width: parent.width
                columnSpacing: 0
                rowSpacing: 0
                columns: Math.floor((width - 40) / 200.0)
                
                FolderListModel {
                    id: folderModel
                    folder: "file:///Users/sarathi/dev/emu/cores/nes/roms"
                    nameFilters: ["*.nes"]
                }
                
                Repeater {
                    model: folderModel
                    
                    Item {
                        SystemPalette { id: systemPalette; colorGroup: SystemPalette.Active }
                        
                        Layout.fillWidth: true
                        height: 300
                        
                        Item {
                            id: rectangle
                            anchors.centerIn: parent
                            width: 200
                            height: parent.height
                            
                            ColumnLayout {
                                anchors.fill: parent
                                anchors.margins: 20
                                spacing: 10
                                
                                Rectangle {
                                    width: 160
                                    height: 210
                                    color: "#000"
                                    radius: 10
                                    
                                    Layout.alignment: Qt.AlignHCenter
                                    
                                    MouseArea {
                                        anchors.fill: parent
                                        onDoubleClicked: fileOpened(filePath)
                                    }
                                }
                                
                                Item {
                                    width: 160
                                    height: 40
                                    
                                    Layout.alignment: Qt.AlignHCenter
                                    
                                    Text {
                                        text: fileName.replace(/\.nes$/, "")
                                        font.pixelSize: 16
                                        font.bold: true
                                        anchors.fill: parent
                                        color: systemPalette.windowText
                                        wrapMode: Text.WordWrap
                                        horizontalAlignment: Text.AlignHCenter
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    FileDialog {
        id: fileDialog
        modality: Qt.WindowModal
        title: "Choose an NES ROM"
        selectExisting: true
        selectMultiple: false
        selectFolder: false
        nameFilters: [ "Nintendo (NES) ROM (*.nes)" ]
        sidebarVisible: true
        onAccepted: {
            var path = fileUrl.toString ();
            path = path.replace (/^(file:\/{2})/, "");
            path = decodeURIComponent (path);
            appWindow.fileOpened (path);
        }
    }
}
